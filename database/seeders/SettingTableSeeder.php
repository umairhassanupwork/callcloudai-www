<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = array(
            array (
                "title"      => "Maintenance Mode",
                "key"        => "maintenance_mode",
                "value"      => "0",
                "message"    => "Sorry for the inconvenience but we're performing some maintenance at the moment",
                "created_at" => now(),
                "updated_at" => now()
            )
        );

        Setting::insert($settings);
    }
}
