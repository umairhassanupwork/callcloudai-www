<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->boolean("is_subscribed")->default(0);
            $table->string("stripe_customer_id")->nullable();
            $table->string("stripe_subscription_id")->nullable();
            $table->timestamp("subscribed_at")->nullable();

            $table->foreignId('subscription_product_id')->nullable()->constrained('products')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('subscription_price_id')->nullable()->constrained('product_prices')->onUpdate('cascade')->onDelete('cascade');

            $table->index('subscription_product_id');
            $table->index('subscription_price_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
};
