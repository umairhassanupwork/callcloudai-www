<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        
        
        Schema::create('industries', function (Blueprint $table) {
            $table->id();
            $table->string('industry');
            $table->timestamps();
        });
        
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('country');
            $table->timestamps();
        });

        Schema::create('regions', function (Blueprint $table) {
            $table->id();
            $table->string('region');
            $table->foreignId('country_id')->nullable()->constrained('countries')->onUpdate('cascade')->onDelete('set null');
            $table->index('country_id');	
            $table->timestamps();
        });


        Schema::table('users', function (Blueprint $table) {
            
            $table->foreignId('industry_id')->nullable()->constrained('industries')->onUpdate('cascade')->onDelete('set null');
            $table->foreignId('region_id')->nullable()->constrained('regions')->onUpdate('cascade')->onDelete('set null');
            
            $table->index('industry_id');
            $table->index('region_id');
        
        });


        
        Schema::create('company_user', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('company_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->index('user_id');
            $table->index('company_id');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
};
