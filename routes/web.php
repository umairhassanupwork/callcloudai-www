<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;


use App\Http\Controllers\AuthController;
use App\Http\Controllers\WorkspaceController;
use App\Models\WaitList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\WaitListSubscribed;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view("coming-soon");
    // return redirect(config('app.frontend_url'));
});

Route::post('/coming-soon-wait-list', function (Request $request) {
    $request->validate(['email' => 'required|email']);
    $waitList = new WaitList;
    $waitList->email = $request->email;
    $waitList->save();

    Mail::to(config('app.admin_email'))
    ->cc(['ronu5879@gmail.com'])
    ->send(new WaitListSubscribed($waitList));

    return response()->json(['email' => $request->email]);
    // return redirect(config('app.frontend_url'));
});

Route::get('/home', function () {
    return redirect(config('app.frontend_url'));
});



 
Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
 
    return redirect(config('app.frontend_url'));
})->middleware(['auth:sanctum', 'signed'])->name('verification.verify');


Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::middleware('auth:sanctum')->get('/logout', [AuthController::class, 'logout'])->name('logout');


Route::post('/forgot-password',[AuthController::class, 'forgotPassword'])->middleware('guest')->name('password.email');
Route::post('/reset-password',[AuthController::class, 'resetPassword'])->middleware('guest')->name('password.update');

Route::get('/reset-password/{token}', function ($token) {
    $email = $_GET['email'];
    return redirect(config('app.frontend_url')."/reset-password/$token?email=$email");
})->middleware('guest')->name('password.reset');

Route::middleware('auth:sanctum')->get('/workspace/{id}/invitation/{memberId}', [WorkspaceController::class, 'acceptInvitation']);