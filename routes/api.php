<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\IndustryController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\WebhookController;
use App\Http\Controllers\BookingPageController;
use App\Http\Controllers\WorkspaceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/maintenance_mode', [PublicController::class, 'getMaintenanceMode']);

Route::middleware(['auth:sanctum'])->get('/profile', function (Request $request) {
    // return $request->user();

    $data = Auth::user()->getData();

    return response()->json($data);
});

Route::middleware(['auth', 'throttle:6,1'])->get('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    
    $data = ['message' => 'Verification link sent!' ];
    return response()->json($data);

})->name('verification.send');

Route::post('/forgot-password',[AuthController::class, 'forgotPassword'])->middleware('guest')->name('password.email');
Route::post('/reset-password',[AuthController::class, 'resetPassword'])->middleware('guest')->name('password.update');

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::middleware('auth:sanctum')->get('/logout', [AuthController::class, 'logout'])->name('logout');
Route::middleware('auth:sanctum')->post('/profile', [AuthController::class, 'updateProfile'])->name('updateProfile');
Route::middleware('auth:sanctum')->post('/update-password', [AuthController::class, 'updatePassword'])->name('updatePassword');
Route::middleware('auth:sanctum')->get('/delete-account', [AuthController::class, 'deleteAccount'])->name('deleteAccount');

Route::group(['middleware'=>'auth:sanctum'], function() {
    Route::get('/timezones', [AuthController::class, 'getTimezones']);
});

Route::get('/test', [AuthController::class, 'test']);
Route::middleware('auth:sanctum')->get('/industries', [IndustryController::class, 'getList']);

//Workspace related routes
Route::middleware('auth:sanctum')->post('/workspaces', [WorkspaceController::class, 'getList']);
Route::middleware('auth:sanctum')->post('/workspace', [WorkspaceController::class, 'create']);
Route::middleware('auth:sanctum')->post('/bulk-workspaces', [WorkspaceController::class, 'bulkWorkspaces']);
Route::middleware('auth:sanctum')->post('/workspace-bulk-invite', [WorkspaceController::class, 'workspaceBulkInvite']);
Route::middleware('auth:sanctum')->get('/workspace/{id}', [WorkspaceController::class, 'getWorkspace']);
Route::middleware('auth:sanctum')->get('/mark-workspace-as-accessed-now/{id}', [WorkspaceController::class, 'markWorkspaceAsAccessedNow']);
Route::middleware('auth:sanctum')->get('/recently-accessed-workspaces', [WorkspaceController::class, 'recentlyAccessedWorkspaces']);
Route::middleware('auth:sanctum')->post('/workspace/{id}', [WorkspaceController::class, 'update']);
Route::middleware('auth:sanctum')->delete('/workspace/{id}', [WorkspaceController::class, 'delete']);
Route::middleware('auth:sanctum')->post('/workspace/{id}/invite', [WorkspaceController::class, 'inviteUser']);
Route::middleware('auth:sanctum')->post('/workspace/{id}/users', [WorkspaceController::class, 'getUsers']);
Route::middleware('auth:sanctum')->delete('/workspace/delete-member/{id}', [WorkspaceController::class, 'deleteMemberFromWorkspace']);
Route::middleware('auth:sanctum')->post('/workspace/update-member/{id}', [WorkspaceController::class, 'updateMemberInWorkspace']);


//Billing related routes
Route::middleware('auth:sanctum')->get('/get-active-plans', [AdminController::class, 'getActivePlanDetails']);
Route::middleware('auth:sanctum')->get('/get-client-secret', [SubscriptionController::class, 'createSetupIntent']);
Route::middleware('auth:sanctum')->post('/create-free-plan-subscription', [SubscriptionController::class, 'createFreePlanSubscription']);
Route::middleware('auth:sanctum')->post('/invoices', [SubscriptionController::class, 'getInvoices']);
Route::middleware('auth:sanctum')->get('/current-plan-details', [SubscriptionController::class, 'currentPlanDetails']);
Route::middleware('auth:sanctum')->post('/change-plan', [SubscriptionController::class, 'changeSubscription']);
Route::middleware('auth:sanctum')->get('/cancel-subscription', [SubscriptionController::class, 'cancelSubscription']);
Route::middleware('auth:sanctum')->get('/activate-canceled-subscription', [SubscriptionController::class, 'activateCanceledSubscription']);
Route::middleware('auth:sanctum')->get('/payment-methods', [SubscriptionController::class, 'getPaymentMethods']);
Route::middleware('auth:sanctum')->delete('/payment-method/{id}', [SubscriptionController::class, 'deletePaymentMethod']);
Route::middleware('auth:sanctum')->post('/default-payment-method/{id}', [SubscriptionController::class, 'defaultPaymentMethod']);
Route::middleware('auth:sanctum')->get('/invoice/{id}', [SubscriptionController::class, 'getInvoicePDFLink']);
Route::middleware('auth:sanctum')->get('/upcoming-invoice', [SubscriptionController::class, 'getUpcomingInvoice']);
Route::middleware('auth:sanctum')->post('/pay-invoice', [SubscriptionController::class, 'payInvoice']);

// Route::group(['middleware' => ['auth:sanctum']], function() {
    /* For Feedback */
    Route::post('/feedbacks/create', [FeedbackController::class, 'create']);

    /* For Notification */
    Route::get('/user_notification/{user_id}', [NotificationController::class, 'getSingleNotification']);
    Route::post('/notification/{id}', [NotificationController::class, 'markAsRead']);
// });

//Booking related routes
Route::middleware('auth:sanctum')->post('/create-booking-page', [BookingPageController::class, 'create']);
Route::middleware('auth:sanctum')->post('/get-booking-pages', [BookingPageController::class, 'getBookingPages']);

Route::middleware('auth:sanctum')->delete('/booking-page/{id}', [BookingPageController::class, 'deleteBookingPage']);


// webhook route
Route::any('/webhook/stripe', [WebhookController::class, 'stripe']);


// Admin routes
Route::prefix('admin')->group(function () {
    Route::middleware(['auth:sanctum','throttle:6,1','IsAdmin'])->get('/resend-otp', [AuthController::class, 'resendOTP']);
    Route::middleware(['auth:sanctum','IsAdmin'])->post('/verify-otp', [AuthController::class, 'verifyOTP']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/workspaces', [AdminController::class, 'getWorkspaces']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/users', [AdminController::class, 'getUsers']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/companies', [AdminController::class, 'getCompanies']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/plans', [AdminController::class, 'getPlans']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/plan', [AdminController::class, 'createPlan']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/plan/{id}', [AdminController::class, 'updatePlan']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/plan-prices/{id}', [AdminController::class, 'getPlanPrices']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/create-plan-price/{id}', [AdminController::class, 'createPlanPrice']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/plan-price/{id}', [AdminController::class, 'updatePlanPrice']);

    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/create-plan-feature/{id}', [AdminController::class, 'createPlanFeature']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/plan-feature/{id}', [AdminController::class, 'updatePlanFeature']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->get('/plan-features/{id}', [AdminController::class, 'getPlanFeatures']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->delete('/plan-feature/{id}', [AdminController::class, 'deletePlanFeature']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/order-plan-features/{id}', [AdminController::class, 'orderPlanFeatures']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->get('/get-active-plans', [AdminController::class, 'getActivePlanDetails']);
    Route::middleware(['auth:sanctum','OTPVerified','IsAdmin'])->post('/order-products', [AdminController::class, 'orderProducts']);

    // Route::group(['middleware' => ['auth:sanctum', 'OTPVerified', 'IsAdmin']], function() {
        /* For Countries */
        Route::post('/countries', [AdminController::class, 'getCountries']);
        Route::post('/country/{id}', [AdminController::class, 'updateCountry']);

        /* For Timezones */
        Route::post('/timezones', [AdminController::class, 'getTimezones']);
        Route::post('/timezones/create', [AdminController::class, 'createTimezone']);
        Route::post('/timezone/{id}', [AdminController::class, 'updateTimezone']);

        /* For Booking Simulator */
        Route::post('/bookings', [AdminController::class, 'getBookings']);
        Route::post('/bookings/create', [AdminController::class, 'createBooking']);
        Route::post('/booking/{id}', [AdminController::class, 'updateBooking']);

        /* For Feedback */
        Route::post('/feedbacks', [AdminController::class, 'getFeedbacks']);

        /* For Notifications */
        Route::post('/notifications', [AdminController::class, 'getNotifications']);
        Route::post('/notifications/create', [AdminController::class, 'createNotification']);
        Route::post('/notification/{id}', [AdminController::class, 'updateNotification']);

        /* Members List for dropdown */
        Route::get('/members_list', [AdminController::class, 'getMembersList']);

        /* For Setting */
        Route::get('/settings', [AdminController::class, 'getSettings']);
        Route::post('/setting/update', [AdminController::class, 'updateSetting']);
    // });
});



