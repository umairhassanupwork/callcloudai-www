<?php
namespace App\Console;
use DB;
use App\Models\User;
use App\Models\Product;

use Illuminate\Support\Facades\Mail;
use App\Mail\FreePlanWillExpireToday;


class FreeSubscriptionExpiry
{
    public function __invoke()
    {
        $freePlans = Product::where("is_free_plan", 1)->get();
        $freePlansIds = [];
        $freePlanByPlanId = [];
        $cnt = 0;

        foreach($freePlans as $freePlan) {

            $freePlansIds[] = $freePlan->id;
            $freePlanByPlanId[$freePlan->id] = $freePlan;

        }

        $freeUsers = User::whereIn("subscription_product_id", $freePlansIds)->get();

        foreach($freeUsers as $user){

            $secondsOfOneDay = 60 * 60 * 24;
            $nowPlusOneDay = time() + $secondsOfOneDay;
            $subscribed_at = strtotime($user->subscribed_at);
            $datediff = $nowPlusOneDay - $subscribed_at;
            $freeSubscriptionWillExpireToday = 0 <= $datediff - $freePlanByPlanId[$user->subscription_product_id]->trial_days * $secondsOfOneDay && $datediff - $freePlanByPlanId[$user->subscription_product_id]->trial_days * $secondsOfOneDay <= $secondsOfOneDay ;
            
            if ($freeSubscriptionWillExpireToday) {

                $cnt++;

                $willExpireAt = $subscribed_at + $freePlanByPlanId[$user->subscription_product_id]->trial_days * $secondsOfOneDay;
                $willExpireAtDate = date(config("app.date_format"), $willExpireAt);
                
                //send mail to user for free plan will expire today notification
                Mail::to($user->email)
                        ->cc(['ronu5879@gmail.com'])
                        ->send(new FreePlanWillExpireToday($user, $willExpireAtDate));

            }
        }

        echo  "Total trial will expire today => $cnt";
    }
}
