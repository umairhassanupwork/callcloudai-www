<?php

if (! function_exists('encryptId')) {
    
    function encryptId($id) {

        $secretKey = env("APP_KEY");
        $secretIv = env("APP_NAME");

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash("sha256", $secretKey);

        $iv = substr(hash("sha256", $secretIv), 0, 16);

        $output = openssl_encrypt($id, $encrypt_method, $key, 0, $iv);

        return base64_encode($output);
    }
}


if (! function_exists('decryptId')) {
    
    function decryptId($id) {

        $secretKey = env("APP_KEY");
        $secretIv = env("APP_NAME");

        $encrypt_method = "AES-256-CBC";
        $key = hash("sha256", $secretKey);

        $iv = substr(hash("sha256", $secretIv), 0, 16);

        return openssl_decrypt(base64_decode($id), $encrypt_method, $key, 0, $iv);
    }
}
