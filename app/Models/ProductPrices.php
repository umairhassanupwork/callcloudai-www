<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductPrices extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'price_id',
        'cycle',
        'active'
    ];
    
    public function getEncIdAttribute() {
        return encryptId($this->id);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getData()
    {
        $data['id'] = $this->enc_id;
        $data['cycle'] = $this->cycle;
        $data['amount'] = $this->amount;
        $data['active'] = $this->active ? true : false;
        $data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));
        
        //$data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));

        return $data;
    }
}
