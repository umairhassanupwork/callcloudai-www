<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Workspace extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];
    
    public function getEncIdAttribute() {
        return encryptId($this->id);
    }

    // public function regions()
    // {
    //     return $this->hasMany(Region::class);
    // }


    public function getData()
    {
        $data['name'] = $this->name;
        $data['logo'] = $this->logo;
        $data['createdAt'] = date(config("app.date_format"), strtotime($this->created_at));

        if ($this->pivot && $this->pivot->joined_at) {
            $data['joinedAt'] = date(config("app.date_format"), strtotime($this->pivot->joined_at));
        }
        if ($this->pivot && $this->pivot->last_accessed_at) {
            $data['lastAccessedAt'] = date(config("app.date_format"), strtotime($this->pivot->last_accessed_at));
        }
        
        if ($this->pivot && $this->pivot->id) {
            $data['invitationId'] = encryptId($this->pivot->id);
        }

        $data['id'] = $this->enc_id;
        $data['users'] = $this->users_count ? $this->users_count : $this->users->count() ;



        return $data;
    }


    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot(['joined_at','id','nickname','last_accessed_at']);

    }



}
