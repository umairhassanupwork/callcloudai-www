<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class LoginOTP extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = "login_otp";
    protected $fillable = [
        'otp',
        'user_id'
    ];


}
