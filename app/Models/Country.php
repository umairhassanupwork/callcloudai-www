<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Country extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'country', 'created_at', 'updated_at'
        // 'name', 'code', 'created_at', 'updated_at'
    ];
    
    public function getEncIdAttribute() 
    {
        return encryptId($this->id);
    }

    public function regions()
    {
        return $this->hasMany(Region::class);
    }

    public function timezone()
    {
        return $this->hasMany(Timezone::class);
    }

    public function getData()
    {
        $data['id']        = $this->enc_id;
        $data['country']   = $this->country;
        // $data['name']      = $this->name;
        // $data['code']      = $this->code;
        // $data['is_dst']    = $this->is_dst;
        // $data['dst_start'] = $this->dst_start;
        // $data['dst_end']   = $this->dst_end;
        // $data['shift']     = $this->shift;
        $data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));

        return $data;
    }
}
