<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

use App\Notifications\VerifyEmailQueued;
use App\Notifications\ResetPassword as ResetPasswordNotification;


class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'company_name',
        'company_size',
        'role',
        'avatar',
        'city'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'profile_completed_at' => 'datetime',
    ];

    // Changed for send via queue
    public function sendEmailVerificationNotification(){
        $this->notify(new VerifyEmailQueued);
    }

    // Changed for send via queue
    public function sendPasswordResetNotification($token){
        $this->notify(new ResetPasswordNotification($token));
    }


    // Get name as full name
    public function getNameAttribute(){

        if ($this->first_name && $this->last_name) {
            return "{$this->first_name} {$this->last_name}";
        } else {
            $emailPrefix = explode("@", $this->email)[0];
            return $emailPrefix;
        }
    }

    public function getEncIdAttribute() {
        return encryptId($this->id);
    }


    public function industry()
    {
        return $this->belongsTo(Industry::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function workspaces()
    {
        return $this->belongsToMany(Workspace::class)->withPivot(['joined_at','id','nickname','last_accessed_at'])->whereNotNull("joined_at");
    }

    public function timezone()
    {
        return $this->belongsTo(Timezone::class);
    }

    public function getCompanyWorkspaces() {
        if ($this->role != "company") {
            return [];
        } else {
            $workspaces = Workspace::where("company_id",$this->id)->get();
            return $workspaces;
        }
    }

    public function companyWorkspaces()
    {
        return $this->hasMany(Workspace::class, 'company_id' );
    }


    public function getCountryAttribute()
    {
        // return $this->hasOneThrough(Country::class, Region::class);
        if($this->region && $this->region->country) {
            return $this->region->country->country;
        }else{
            return null;
        }
    }


    public function getData()
    {

        $data['id'] = $this->enc_id;
        $data['name'] = $this->name;
        $data['firstName'] = $this->first_name;
        $data['lastName'] = $this->last_name;
        $data['companyName'] = $this->company_name;
        $data['companySize'] = $this->company_size;
        $data['role'] = $this->role;
        $data['email'] = $this->email;
        $data['avatar'] = $this->avatar ? $this->avatar : null;
        $data['emailVerified'] = $this->email_verified_at ? true : false;
        $data['profileCompleted'] = $this->profile_completed_at ? true : false;
        $data['country'] = $this->country;
        $data['city'] = $this->city;
        $data['region'] = $this->region ? $this->region->region : null;
        $data['timezone'] = $this->timezone ?? null;
        $data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));

        if ($this->role == "member" && $this->pivot && $this->pivot->joined_at) {
            $data["joinedAt"] = date(config("app.date_format"), strtotime($this->pivot->joined_at));
        } else {
            $data["joinedAt"] = null;
        }
        
        if ($this->role == "member" && $this->pivot && $this->pivot->last_accessed_at) {
            $data["lastAccessedAt"] = date(config("app.date_format"), strtotime($this->pivot->last_accessed_at));
        } else {
            $data["lastAccessedAt"] = null;
        }
        
        if ($this->role == "member" && $this->pivot && $this->pivot->id) {
            $data["invitationId"] =  encryptId($this->pivot->id);
        } else {
            $data["invitationId"] = null;
        }
        
        if ($this->role == "member" && $this->pivot && $this->pivot->nickname) {
            $data["nickname"] =  $this->pivot->nickname;
        } else {
            $data["nickname"] = null;
        }
        
        $data['industry'] = $this->industry ? [
            "industry" => $this->industry->industry,
            "id" => $this->industry->id
        ] : null;

        $data['isSubscribed'] = $this->is_subscribed ? true : false;
        $data['stripeSubscriptionStatus'] = $this->stripe_subscription_status;
        $data['hasActiveSubscription'] = $this->is_subscribed && $this->stripe_subscription_status == "active";

        $data['hasFreeSubscription'] = false;

        if (!$data['hasActiveSubscription'] && $this->is_subscribed ) {
            $plan = Product::find($this->subscription_product_id);

            if ($plan && $plan->is_free_plan) {
                $data['hasFreeSubscription'] = true;
                $now = time();
                $subscribed_at = strtotime($this->subscribed_at);
                $datediff = $now - $subscribed_at;
                $data['hasFreeSubscriptionExpired'] = $plan->trial_days - round($datediff / (60 * 60 * 24)) < 0;
            }
        }


        if ($this->role == "admin") {
            // $data["otpVerified"] = session('optVerified', false);
            $data["otpVerified"] = true;
        }

        return $data;
    }

    public function getMemberDropdown()
    {
        $data['id']    = $this->enc_id;
        $data['label'] = $this->name;
        $data['value'] = $this->enc_id;
       
        return $data;
    }
    
    public function getCompanyData()
    {

        $data['id'] = $this->enc_id;
        $data['name'] = $this->name;
        $data['firstName'] = $this->first_name;
        $data['lastName'] = $this->last_name;
        $data['companyName'] = $this->company_name;
        $data['companySize'] = $this->company_size;
        $data['role'] = $this->role;
        $data['email'] = $this->email;
        $data['avatar'] = $this->avatar ? $this->avatar : null;
        $data['emailVerified'] = $this->email_verified_at ? true : false;
        $data['profileCompleted'] = $this->profile_completed_at ? true : false;
        $data['country'] = $this->country;
        $data['city'] = $this->city;
        $data['region'] = $this->region ? $this->region->region : null;
        $data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));

        if ($this->role == "member" && $this->pivot && $this->pivot->joined_at) {
            $data["joinedAt"] = date(config("app.date_format"), strtotime($this->pivot->joined_at));
        } else {
            $data["joinedAt"] = null;
        }
        
        if ($this->role == "member" && $this->pivot && $this->pivot->last_accessed_at) {
            $data["lastAccessedAt"] = date(config("app.date_format"), strtotime($this->pivot->last_accessed_at));
        } else {
            $data["lastAccessedAt"] = null;
        }
        
        if ($this->role == "member" && $this->pivot && $this->pivot->id) {
            $data["invitationId"] =  encryptId($this->pivot->id);
        } else {
            $data["invitationId"] = null;
        }
        
        if ($this->role == "member" && $this->pivot && $this->pivot->nickname) {
            $data["nickname"] =  $this->pivot->nickname;
        } else {
            $data["nickname"] = null;
        }
        
        $data['industry'] = $this->industry ? [
            "industry" => $this->industry->industry,
            "id" => $this->industry->id
        ] : null;

        if ($this->role == "admin") {
            $data["otpVerified"] = session('optVerified', false);
        }

        $data['workspaces'] = $this->company_workspaces_count ? $this->company_workspaces_count :  0;

        $data['isSubscribed'] = $this->is_subscribed ? true : false;
        $data['stripeSubscriptionStatus'] = $this->stripe_subscription_status;
        $data['hasActiveSubscription'] = $this->is_subscribed && $this->stripe_subscription_status == "active";

        $data['hasFreeSubscription'] = false;
        $data['hasFreeSubscriptionExpired'] = false;

        if (!$data['hasActiveSubscription'] && $this->is_subscribed ) {
            $plan = Product::find($this->subscription_product_id);

            if ($plan && $plan->is_free_plan) {
                $data['hasFreeSubscription'] = true;
                $now = time();
                $subscribed_at = strtotime($this->subscribed_at);
                $datediff = $now - $subscribed_at;
                $data['hasFreeSubscriptionExpired'] = $plan->trial_days - round($datediff / (60 * 60 * 24)) < 0;
            }
        }

        return $data;
    }



}
