<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SetupIntent extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'setup_intent',
    ];
    
    public function getEncIdAttribute() {
        return encryptId($this->id);
    }

    /**
     * Get the user that owns the SetupIntent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getData()
    {
        
        $data['id'] = $this->enc_id;
        $data['setup_intent'] = $this->setup_intent;
        $data['user_id'] = $this->user->enc_id;
        $data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));

        return $data;
    }

}
