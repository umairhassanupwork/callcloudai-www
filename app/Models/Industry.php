<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'industry',
    ];
    

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function getIdAttribute($id)
    {
        return encryptId($id);
    }



}
