<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];
    
    public function getEncIdAttribute() {
        return encryptId($this->id);
    }

    public function prices()
    {
        return $this->hasMany(ProductPrices::class);
    }
    
    public function activePrices()
    {
        return $this->hasMany(ProductPrices::class)->where('active',1);
    }

    public function features()
    {
        return $this->hasMany(ProductFeatures::class)->orderBy("order_number");
    }

    public function getData()
    {
        $data['id'] = $this->enc_id;
        $data['name'] = $this->name;
        $data['description'] = $this->description;
        $data['order_number'] = $this->order_number;
        $data['is_free_plan'] = $this->is_free_plan ? true : false;
        $data['trial_days'] = $this->trial_days;
        $data['active'] = $this->active ? true : false;
        $data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));

        if ($this->features) {
            $features = [];

            foreach($this->features as $feature) {
                $features[] = $feature->getData();
            }

            $data['features'] = $features;
        }
        
        if ($this->activePrices) {
            $active_prices = [];

            foreach($this->activePrices as $active_price) {
                $active_prices[] = $active_price->getData();
            }

            $data['active_prices'] = $active_prices;
        }
        return $data;
    }

}
