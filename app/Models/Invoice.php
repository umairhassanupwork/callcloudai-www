<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Invoice extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];
    
    public function getEncIdAttribute() {
        return encryptId($this->id);
    }

    /**
     * Get the user that owns the Invoice
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    

    public function getData()
    {
        $data['id'] = $this->enc_id;
        $data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));
        $data["status"] = $this->status;
        $data["total"] = $this->amount;
        $data["stripe_payment_status"] = $this->stripe_payment_status;
        return $data;
    }

}
