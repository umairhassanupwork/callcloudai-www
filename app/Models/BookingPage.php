<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BookingPage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];
    
    public function getEncIdAttribute() 
    {
        return encryptId($this->id);
    }

    public function createdBy()
    {
        return $this->BelongsTo(User::class, 'created_by');
    }
    
    public function workspace()
    {
        return $this->BelongsTo(Workspace::class);
    }


    public function getData()
    {
        $data['id'] = $this->enc_id;
        $data['title'] = $this->title;
        $data['slug'] = $this->slug;
        $data['logo'] = $this->logo;
        $data['intro'] = $this->intro;
        $data['created_by'] = $this->createdBy->enc_id;
        $data['workspace_id'] = $this->workspace->enc_id;
        $data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));

        return $data;
    }
}
