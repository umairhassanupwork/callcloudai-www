<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'key', 'value', 'message'
    ];

    public function getEncIdAttribute() 
    {
        return encryptId($this->id);
    }

    public function getData()
    {
        $data['id']         = $this->enc_id;
        $data['title']      = $this->title;
        $data['key']        = $this->key;
        $data['value']      = $this->value;
        $data['message']    = $this->message ?? '';

        return $data;
    }
}
