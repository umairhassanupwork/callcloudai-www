<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class WaitList extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
    ];

    protected $table = "wait_list";



}
