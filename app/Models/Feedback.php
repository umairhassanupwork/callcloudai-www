<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use HasFactory;

    protected $table = 'feedbacks';

    protected $fillable = [
        'user_id', 'rating', 'message', 'created_at', 'updated_at'
    ];

    public function getEncIdAttribute() 
    {
        return encryptId($this->id);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getData()
    {
        $data['id']         = $this->enc_id;
        $data['user_name']  = $this->user->first_name.' '.$this->user->last_name;
        $data['rating']     = $this->rating;
        $data['message']    = $this->message;
        $data["createdAt"]  = date(config("app.date_format"), strtotime($this->created_at));

        return $data;
    }
}
