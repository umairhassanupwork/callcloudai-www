<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductFeatures extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
    ];
    
    public function getEncIdAttribute() {
        return encryptId($this->id);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }


    public function getData()
    {
        $data['id'] = $this->enc_id;
        $data['title'] = $this->title;
        $data['order_number'] = $this->order_number;
        $data['active'] = $this->active ? true : false;
        $data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));
        
        //$data["createdAt"] = date(config("app.date_format"), strtotime($this->created_at));

        return $data;
    }
}
