<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'owner_timezone_id', 'recipient_timezone_id', 'start_time', 'end_time', 'created_at', 'updated_at'
    ];

    // protected $dates = [
    //     'start_time', 'end_time', 'created_at', 'updated_at'
    // ];

    // protected $fillable = [
    //     'user_id', 'duration', 'start_time', 'end_time', 'created_at', 'updated_at'
    // ];

    public function getEncIdAttribute() 
    {
        return encryptId($this->id);
    }

    public function ownerTimezone()
    {
        return $this->belongsTo(Timezone::class, 'owner_timezone_id');
    }

    public function recipientTimezone()
    {
        return $this->belongsTo(Timezone::class, 'recipient_timezone_id');
    }

    public function getData()
    {
        $data['id']                 = $this->enc_id;
        $data['name']               = $this->name;

        $data['owner_timezone'] = $this->ownerTimezone ? [
            "name" => $this->ownerTimezone->name,
            "id" => $this->ownerTimezone->id
        ] : null;

        $data['recipient_timezone'] = $this->recipientTimezone ? [
            "name" => $this->recipientTimezone->name,
            "id" => $this->recipientTimezone->id
        ] : null;

        $data['start_time'] = (new \DateTime(($this->start_time), new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->ownerTimezone->identifier))->format('Y-m-d H:i:s');
        $data['end_time']   = (new \DateTime(($this->end_time), new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->ownerTimezone->identifier))->format('Y-m-d H:i:s');

        $data['start_time_owner'] = (new \DateTime(($this->start_time), new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->ownerTimezone->identifier))->format(config("app.date_format"));
        $data['end_time_owner']   = (new \DateTime(($this->end_time), new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->ownerTimezone->identifier))->format(config("app.date_format"));

        $data['start_time_recipient'] = (new \DateTime(($this->start_time), new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->recipientTimezone->identifier))->format(config("app.date_format"));
        $data['end_time_recipient']   = (new \DateTime(($this->end_time), new \DateTimeZone('UTC')))->setTimezone(new \DateTimeZone($this->recipientTimezone->identifier))->format(config("app.date_format"));

        $data["createdAt"]          = date(config("app.date_format"), strtotime($this->created_at));

        return $data;
    }
}
