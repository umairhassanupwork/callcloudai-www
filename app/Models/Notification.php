<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'type', 'read_at', 'message', 'created_at', 'updated_at'
    ];

    public function getEncIdAttribute() 
    {
        return encryptId($this->id);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getData()
    {
        $data['id']          = $this->enc_id;
        $data['type']        = $this->type;
        $data['message']     = $this->message;
        $data["createdAt"]   = date(config("app.date_format"), strtotime($this->created_at));

        return $data;
    }
}
