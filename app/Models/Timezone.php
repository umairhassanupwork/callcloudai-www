<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'identifier', 'group', 'created_at', 'updated_at'
    ];

    public function getEncIdAttribute() 
    {
        return encryptId($this->id);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getData()
    {
        $data['id']         = $this->enc_id;
        $data['name']       = $this->name;
        $data['identifier'] = $this->identifier;
        $data['group']      = $this->group;
        $data["createdAt"]  = date(config("app.date_format"), strtotime($this->created_at));

        return $data;
    }
}
