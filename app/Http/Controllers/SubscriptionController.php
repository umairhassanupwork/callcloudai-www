<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductPrices;
use App\Models\Invoice;
use App\Models\SetupIntent;
use Auth;



class SubscriptionController extends Controller
{
    public function createSetupIntent(Request $request) {

        $user = Auth::user();
        $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));

        //create customer in stripe if not created
        if (!$user->stripe_customer_id) {

            $customer = $stripe->customers->create([
                'name' => $user->name,
                'email' => $user->email,
                'description' => "$user->name from $user->company_name",
                'metadata' => [
                    'user_id' => $user->id
                ]
            ]);

            $user->stripe_customer_id = $customer->id;
            $user->save();
        }
          
        $intent = $stripe->setupIntents->create(
            [
              'customer' => $user->stripe_customer_id,
              'payment_method_types' => ['card'],
            ]
        );

        $data['client_secret'] = $intent->client_secret;
        return response()->json($data);
    }


    public function createFreePlanSubscription(Request $request) {

        $request->validate([
            'plan_id' => 'required|string'
        ]);

        $plan = Product::find(decryptId($request->plan_id));

        if (!$plan || !$plan->is_free_plan ) {
            abort(422, "You can't perform this action");
        }

        $user = Auth::user();

        $user->subscription_product_id = $plan->id;
        $user->subscribed_at = now();
        $user->is_subscribed = 1;

        $user->save();

        return response()->json(["message" => "Subscription created successfully!"]);
          

    }

    public function getInvoices(Request $request) {

        $request->validate([
            'perPage' => 'required|integer',
            'page' => 'required|integer',

        ]);

        $user = Auth::user();
        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset = ($request->page - 1) * $perPage;

        $invoicesQuery = Invoice::where("user_id", $user->id);

        if ($request->input('sortColumn')) {
            if($request->sortColumn == "createdAt"){
                $invoicesQuery = $invoicesQuery->orderBy('created_at', $request->sort);
            }
        }
         

        $invoices = $invoicesQuery->offset($offset)->limit($perPage)->get();
        
        $invoicesArray = [];

        foreach($invoices as $invoice) {
            $invoicesArray[] = $invoice->getData();
        }

        // $invoicesArray[] = ['id' => "1", 'status' => 'open', 'total' => 123, 'stripe_payment_status' => 'payment_action_required', 'createdAt' => date(config("app.date_format"), time())];
        // $invoicesArray[] = ['id' => "2", 'status' => 'paid', 'total' => 123, 'stripe_payment_status' => null, 'createdAt' => date(config("app.date_format"), time())];
        // $invoicesArray[] = ['id' => 2, 'createdAt' => date(config("app.date_format"), time())];
        // $invoicesArray[] = ['id' => 3, 'createdAt' => date(config("app.date_format"), time())];

        
        $total = Invoice::where("user_id", $user->id)->count();

        return response()->json(["invoices" => $invoicesArray, "total" => $total]);

    }


    public function currentPlanDetails() {
        // subscription_product_id
        // subscription_price_id
        // is_subscribed
        // stripe_customer_id
        // stripe_subscription_id
        // subscribed_at

        $user = Auth::user();

        $currentPlan = Product::find($user->subscription_product_id);

        if (!$currentPlan) {
            abort(422, "You have not any active plan");
        }

        if ($currentPlan->is_free_plan) {

            $data["currentPlan"] = $currentPlan->getData();
            $data["currentPlan"]["subscribed_at"] = $user->subscribed_at;
            $data["currentPlan"]["active_until"] = date(config("app.date_format"), strtotime($user->subscribed_at. " + $currentPlan->trial_days days"));
            $data["currentPlan"]["days"] = $currentPlan->trial_days;
            $now = time(); // or your date as well
            $subscribed_at = strtotime($user->subscribed_at);
            $datediff = $now - $subscribed_at;
            $data["currentPlan"]["remainingDays"] = $currentPlan->trial_days - round($datediff / (60 * 60 * 24));

            $data["currentPlan"]['isSubscribed'] = $user->is_subscribed ? true : false;
            // $data['stripeSubscriptionStatus'] = $this->stripe_subscription_status;
            // $data['hasActiveSubscription'] = $this->is_subscribed && $this->stripe_subscription_status == "active";

            $data["currentPlan"]['hasFreeSubscription'] = true;
            $now = time();
            $subscribed_at = strtotime($user->subscribed_at);
            $datediff = $now - $subscribed_at;
            $data["currentPlan"]['hasFreeSubscriptionExpired'] = $currentPlan->trial_days - round($datediff / (60 * 60 * 24)) < 0;
            

        } else {

            if (!$user->stripe_subscription_id) {
                abort(422, "You have not any active subscription");
            }

            $currentPrice = ProductPrices::find($user->subscription_price_id);

            $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
            $subscription = $stripe->subscriptions->retrieve(
                $user->stripe_subscription_id,
                []
            );
            
            $customer = $stripe->customers->retrieve(
                $user->stripe_customer_id,
                []
            );
              
              
            $data["currentPlan"] = $currentPlan->getData();
            $data["currentPlan"]["current_period_start"] = $subscription->current_period_start;
            $data["currentPlan"]["current_period_end"] = $subscription->current_period_end;
            $data["currentPlan"]["currentPrice"] = $currentPrice->getData();
            $data["currentPlan"]["subscribed_at"] = $user->subscribed_at;


            $data["currentPlan"]["active_until"] = date(config("app.date_format"), $subscription->current_period_end);
            $data["currentPlan"]["days"] = round(($subscription->current_period_end - $subscription->current_period_start) / (60 * 60 * 24));

            $now = time(); // or your date as well
            $datediff = $subscription->current_period_end - $now;
            $data["currentPlan"]["remainingDays"] = round($datediff / (60 * 60 * 24));

            $data["currentPlan"]['isSubscribed'] = $user->is_subscribed ? true : false;
            $data["currentPlan"]['stripeSubscriptionStatus'] = $user->stripe_subscription_status;
            $data["currentPlan"]['hasActiveSubscription'] = $user->is_subscribed && $user->stripe_subscription_status == "active";
            $data["currentPlan"]['hasFreeSubscription'] = false;
            // $data["currentPlan"]['cancel_at_period_end'] = $subscription->cancel_at_period_end;
            $data["currentPlan"]["willCancelAt"] = $subscription->cancel_at_period_end ? date(config("app.date_format"), $subscription->current_period_end) : null;
            $data["currentPlan"]["balance"] = $customer->balance ? $customer->balance / 100 : 0;
            
        }

        return response()->json(["currentPlanDetails" => $data]);
    }

    public function changeSubscription(Request $request) {

        $request->validate([
            'plan_id' => 'required|string',
            'price_id' => 'sometimes|string'
        ]);

        $plan = Product::find(decryptId($request->plan_id));
        $user = Auth::user();

        if (!$plan || !$plan->active) {
            abort(422, "You can't perform this action");
        }
        
        if ($plan->is_free_plan) {
            //check and cancel if active subscription
            if ($user->stripe_subscription_id) {
                $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
                $stripe->subscriptions->cancel(
                    $user->stripe_subscription_id,
                    []
                );
            }
            $user->stripe_subscription_id = null;
            $user->subscription_product_id = $plan->id;
            $user->subscription_price_id = null;
            $user->subscribed_at = now();
            $user->save();
            return response()->json(["data" => true, "message" => "Subscribed to free plan!"]);
        } else {
            if (!$user->stripe_customer_id) {
                return response()->json(["message" => "Please add payment method first!"]);
            }

            $price = ProductPrices::find(decryptId($request->price_id));

            if (!$price || !$price->active) {
                abort(422, "You can't perform this action");
            }
            if ($user->stripe_subscription_id && !in_array($user->stripe_subscription_status, ["canceled", "incomplete_expired"])) {

                $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
                $subscription = $stripe->subscriptions->retrieve($user->stripe_subscription_id);

                $stripe->subscriptions->update(
                    $user->stripe_subscription_id,
                    [
                        'items' => [[
                            'id' => $subscription->items->data[0]->id,
                            'price' => $price->price_id
                        ]],
                        'proration_behavior' => 'always_invoice'
                    ]
                );

                $user->subscription_product_id = $plan->id;
                $user->subscription_price_id = $price->id;
                $user->subscribed_at = now();
                $user->save();

                return response()->json(["message" => "Subscription updated successfully!"]);
                
            } else {

                $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
                $paymentMethods =  $stripe->customers->allPaymentMethods(
                    $user->stripe_customer_id,
                    ['type' => 'card']
                );

                if ($paymentMethods->data[0]) {

                    $subscription = $stripe->subscriptions->create(
                        [
                            'customer' => $user->stripe_customer_id,
                            'default_payment_method' => $paymentMethods->data[0]->id,
                            'items' => [['price' => $price->price_id]]
                        ]
                    );

                    return response()->json(["message" => "Subscription created successfully!"]);
                } else {
                    return response()->json(["message" => "Please add payment method first!"]);
                }

            }

        }
          
    }


    public function cancelSubscription(Request $request) {

        $user = Auth::user();

        if ($user->stripe_subscription_id) {

            try {

                $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
                // $stripe->subscriptions->cancel(
                //     $user->stripe_subscription_id,
                //     []
                // );

                $stripe->subscriptions->update(
                    $user->stripe_subscription_id,
                    [
                        "cancel_at_period_end" => true
                    ]
                );
    
                return response()->json(["message" => "Subscription cancelled successfully!"]);
            } catch (\Exception $e) {
                return response()->json(["message" => "Something went wrong!", "e" => $e->getMessage()]);
            }
        } else {
            return response()->json(["message" => "You dont have active subscription!"]);
        }

        // $user->stripe_subscription_id = null;
        // $user->subscription_price_id = null;

        // $freePlan = Product::where("is_free_plan", true)->where("active", true)->first();
        
        // if ($freePlan) {
        //     $user->subscription_product_id = $freePlan->id;
        //     $user->subscribed_at = now();
        //     $user->is_subscribed = 1;
        // } else {
        //     $user->subscription_product_id = null;
        //     $user->subscribed_at = null;
        //     $user->is_subscribed = 0;
        // }

        // $user->save();

        // return response()->json(["message" => "Subscription cancelled successfully!"]);
    }
    
    public function activateCanceledSubscription(Request $request) {

        $user = Auth::user();

        if ($user->stripe_subscription_id) {

            try {

                $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
                $subscription = $stripe->subscriptions->retrieve(
                    $user->stripe_subscription_id,
                    []
                );

                if ($subscription->cancel_at_period_end) {

                    $stripe->subscriptions->update(
                        $user->stripe_subscription_id,
                        [
                            "cancel_at_period_end" => false
                        ]
                    );
        
                    return response()->json(["message" => "Subscription will be continue now!"]);
                }

                abort(422, "You have perform this action");
                
            } catch (\Exception $e) {
                return response()->json(["message" => "Something went wrong!", "e" => $e->getMessage()]);
            }
        } else {
            return response()->json(["message" => "You dont have active subscription!"]);
        }
    }


    public function getPaymentMethods() {

        $user = Auth::user();

        if (!$user->stripe_customer_id) {
        
            return response()->json(['paymentMethods' => []]);
        
        } else {

            $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
            $paymentMethodsResponse =  $stripe->customers->allPaymentMethods(
                $user->stripe_customer_id,
                ['type' => 'card']
            );

            $defaultMethod = null;
            if ($user->stripe_subscription_id) {

                $subscription = $stripe->subscriptions->retrieve(
                    $user->stripe_subscription_id,
                    []
                );

                $defaultMethod = $subscription ? $subscription->default_payment_method : null;
            }
              

            $paymentMethods = [];

            foreach($paymentMethodsResponse->data as $paymentMethod) {
                
                $tmp['expiryDate'] = $paymentMethod->card->exp_month."/".$paymentMethod->card->exp_year;
                $tmp['last4'] = $paymentMethod->card->last4;
                $tmp['brand'] = $paymentMethod->card->brand;
                $tmp['is_primary'] = $paymentMethod->id === $defaultMethod;
                $tmp['id'] = $paymentMethod->id;

                $paymentMethods[] = $tmp;
            }

            return response()->json(['paymentMethods' => $paymentMethods]);
        }
    }

    public function deletePaymentMethod(Request $request, $id) {
        
        $user = Auth::user();

        if (!$user->stripe_customer_id) {
        
            abort(422, "You can't perform this action");
        
        } else {
            
            $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
            $paymentMethodsResponse =  $stripe->customers->allPaymentMethods(
                $user->stripe_customer_id,
                ['type' => 'card']
            );

            $defaultMethod = null;
            if ($user->stripe_subscription_id) {

                $subscription = $stripe->subscriptions->retrieve(
                    $user->stripe_subscription_id,
                    []
                );

                $defaultMethod = $subscription ? $subscription->default_payment_method : null;
            }

            if ($defaultMethod && $defaultMethod == $id) {
                abort(422, "You can't delete primary payment method");
            }

            foreach($paymentMethodsResponse->data as $paymentMethod) {

                if ($paymentMethod->id == $id) {
                    $stripe->paymentMethods->detach(
                        $id,
                        []
                    );
                    
                    return response()->json(['message' => "Payment method deleted successfully!"]);
                }
            }

            abort(422, "You can't perform this action");
        }
    }

    public function defaultPaymentMethod(Request $request, $id) {

        $user = Auth::user();

        if (!$user->stripe_customer_id || !$user->stripe_subscription_id) {
        
            abort(422, "You can't perform this action");
        
        } else {
            
            $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
            $paymentMethodsResponse =  $stripe->customers->allPaymentMethods(
                $user->stripe_customer_id,
                ['type' => 'card']
            );

            $defaultMethod = null;
            $subscription = $stripe->subscriptions->retrieve(
                $user->stripe_subscription_id,
                []
            );

            $defaultMethod = $subscription ? $subscription->default_payment_method : null;


            if ($defaultMethod && $defaultMethod == $id) {
                abort(422, "This is already a default payment method");
            }

            $updated = false;
            foreach($paymentMethodsResponse->data as $paymentMethod) {

                if ($paymentMethod->id == $id) {
                    
                    $stripe->subscriptions->update(
                        $user->stripe_subscription_id,
                        ['default_payment_method' => $paymentMethod->id]
                    );
                    $updated = true;
                    break;
                }
            }

            if ($updated) {
                return response()->json(['message' => "Default payment method updated successfully!"]);
            }

            abort(422, "You can't perform this action");
        }
    }

    public function getInvoicePDFLink(Request $request, $id){

        $user = Auth::user();
        $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));

        $invoice = Invoice::where("id", decryptId($id))->where("user_id", $user->id)->first();

        if (!$invoice) {
            abort(422, "You can't perform this action");
        }

        $invoiceObj = json_decode($invoice->json_data);

        $stripeInvoice = $stripe->invoices->retrieve(
            $invoiceObj->id,
            []
        );

        return response()->json(['invoicePDFLink' => $stripeInvoice->invoice_pdf]);
          
    }

    public function payInvoice(Request $request){

        $request->validate([
            'invoice_id' => 'required|string',
            'payment_method' => 'required|string'
        ]);

        $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
        $user = Auth::user();
        $invoice = Invoice::find(decryptId($request->invoice_id));

        if (!$invoice) {
            abort(422, "You can't perform this action");
        }
        
        if ($invoice->user_id != $user->id) {
            abort(422, "You can't perform this action");
        }

        $paymentMethodsResponse =  $stripe->customers->allPaymentMethods(
            $user->stripe_customer_id,
            ['type' => 'card']
        );

        $hasPaymentMethod = false;
        foreach($paymentMethodsResponse->data as $paymentMethod) {

            if ($paymentMethod->id == $request->payment_method) {
                $hasPaymentMethod = true;
                break;
            }
        }

        if ($hasPaymentMethod) {

            try {

                $invoiceResponse = $stripe->invoices->pay(
                    $invoice->stripe_invoice_id,
                    ["payment_method" => $request->payment_method]
                );
    
                if ($invoiceResponse && $invoiceResponse->status == "paid") {
                    return response()->json(["message" => "Invoice paid successfully!"]);
                } else {
                    return response()->json(["message" => "Invoice not paid yet!", "invoiceResponse" => $invoiceResponse]);
                }

            } catch (\Exception $e) {

                $errorMessage = $e->getMessage();
                abort(422, "Failed, $errorMessage");

            }
              
            return response()->json(["message" => "Invoice not paid yet!"]);
        }

        abort(422, "You can't perform this action");

    }


    public function getUpcomingInvoice(Request $request){

        $user = Auth::user();

        if (!$user->stripe_customer_id || !$user->stripe_subscription_id) {
            abort(422, "You can't perform this action"); 
        }

        $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));

        try {

            $invoice = $stripe->invoices->upcoming([
                'customer' => $user->stripe_customer_id,
                // 'customer' => 'cus_MXqQ7zUxlRADTa',
            ]);
            // $invoice->createdAt = date(config("app.date_format"), $invoice->created);
            $invoiceData["nextPaymentAttempt"] = date(config("app.date_format"), $invoice->next_payment_attempt);
            $invoiceData["total"] = $invoice->total/100;
            // $invoice->createdAt = next_payment_attempt
            return response()->json(['invoice' => $invoiceData]);

        } catch (\Exception $e) {

            return response()->json(['invoice' => null, 'e' => $e->getMessage()]);

        }
          
        return response()->json(['invoice' => null]);
    }
    
}