<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function getMaintenanceMode()
    {
        $maintenance_mode = Setting::where('key', 'maintenance_mode')->first() ?? [];

        return response()->json($maintenance_mode);
    }
}
