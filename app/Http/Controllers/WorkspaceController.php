<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Workspace;
use App\Models\Industry;
use App\Models\User;
use Auth;
use DB;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Builder;

use App\Mail\WorkspaceInvitation;




class WorkspaceController extends Controller
{
    public function create(Request $request) {
        
        $request->validate([
            'name' => 'required|unique:App\Models\Workspace,name,NULL,id,company_id,'.Auth::user()->id,
            'logo' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);

        if (! Gate::allows('create-workspace')) {
            abort(403, "You can't perform this action");
        }

        $workspace = new Workspace;
        $workspace->name = $request->name;
        $workspace->company_id = Auth::user()->id;

        if($request->file('logo')){
            $path = $request->file('logo')->store('workspace_logo','public');
            $workspace->logo = config('app.url').'/storage/'.$path;
        }

        $workspace->save();

        $data['message'] = 'Workspace created successfully!';
        $data['workspace'] = $workspace->getData();

        return response()->json($data);
    }

    public function bulkWorkspaces(Request $request) {


        $request->validate([
            "names"    => "required|array",
            'names.*' => 'required|string|distinct|unique:App\Models\Workspace,name,NULL,id,company_id,'.Auth::user()->id,
        ]);

        if (! Gate::allows('create-workspace')) {
            abort(403, "You can't perform this action");
        }

        foreach ($request->names as $name) {

            $workspace = new Workspace;
            $workspace->name = $name;
            $workspace->company_id = Auth::user()->id;

            $workspace->save();

            $data['workspaces'][] = $workspace->getData();
        }
        $total = count($request->names);
        $message = $total > 1 ? "$total Workspaces created successfully!" : "$total Workspace created successfully!";
        $data['message'] = $message;

        return response()->json($data);
    }
    
    public function update(Request $request, $id) {
        
        $id = decryptId($id);

        $request->validate([
            'name' => 'required|unique:App\Models\Workspace,name,'.$id.',id,company_id,'.Auth::user()->id,
            'logo' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
        
        $workspace = Workspace::find($id);

        if(!$workspace) {
            abort(422, "You can't perform this action");
        }

        if (! Gate::allows('update-workspace', $workspace)) {
            abort(403, "You can't perform this action");
        }

        $workspace->name = $request->name;

        if($request->file('logo')){
            $path = $request->file('logo')->store('workspace_logo','public');
            $workspace->logo = config('app.url').'/storage/'.$path;
        }

        $workspace->save();

        $data['message'] = 'Workspace updated successfully!';
        $data['workspace'] = $workspace->getData();

        return response()->json($data);
    }
    
    
    public function delete(Request $request, $id) {
        
        $workspace = Workspace::find(decryptId($id));

        if(!$workspace) {
            abort(422, "You can't perform this action");
        }
        
        if (! Gate::allows('delete-workspace', $workspace)) {
            abort(403, "You can't perform this action");
        }

        $workspace->delete();


        $data['message'] = 'Workspace deleted successfully!';

        return response()->json($data);
    }


    public function getList(Request $request) {

        $request->validate([
            'perPage' => 'required|integer',
            'page' => 'required|integer',

        ]);

        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset = ($request->page - 1) * $perPage;

        $user = Auth::user();

        if ($user->role == "member") {
            
            $workspacesQuery = $user->workspaces();

            if ($request->input("q")) {
                $workspacesQuery = $workspacesQuery->where('name', 'like' ,"%".$request->q."%");
            }

            if ($request->input('sortColumn')) {
                if($request->sortColumn == "joinedAt"){
                    $workspacesQuery = $workspacesQuery->orderBy('joined_at', $request->sort);
                }
                if($request->sortColumn == "name"){
                    $workspacesQuery = $workspacesQuery->orderBy('name', $request->sort);
                }
            }
            

            $workspaces = $workspacesQuery->offset($offset)
            ->limit($perPage)
            ->get();
            
            $workspacesArray = [];

            foreach($workspaces as $workspace) {
                $workspacesArray[] = $workspace->getData();
            }
            
            if ($request->input("q")) {
                $total = $user->workspaces()->where('name', 'like' ,"%".$request->q."%")->count();
            } else {
                $total = $user->workspaces()->count();
            }
            
            return response()->json(["workspaces" => $workspacesArray, "total" => $total]);
        }

        $workspacesQuery = Workspace::withCount('users')
        ->where("company_id", Auth::user()->id);

        if ($request->input("q")) {
            $workspacesQuery = $workspacesQuery->where('name', 'like' ,"%".$request->q."%");
        }

        if ($request->input('sortColumn')) {
            if($request->sortColumn == "createdAt"){
                $workspacesQuery = $workspacesQuery->orderBy('created_at', $request->sort);
            }
            if($request->sortColumn == "name"){
                $workspacesQuery = $workspacesQuery->orderBy('name', $request->sort);
            }
            if($request->sortColumn == "users"){
                $workspacesQuery = $workspacesQuery->orderBy('users_count', $request->sort);
            }
         }
         

        $workspaces = $workspacesQuery->offset($offset)
        ->limit($perPage)
        ->get();
        
        $workspacesArray = [];

        foreach($workspaces as $workspace) {
            $workspacesArray[] = $workspace->getData();
        }
        
        

        if ($request->input("q")) {
            $total = Workspace::where("company_id",Auth::user()->id)->where('name', 'like' ,"%".$request->q."%")->count();
        } else {
            $total = Workspace::where("company_id",Auth::user()->id)->count();
        }

        return response()->json(["workspaces" => $workspacesArray, "total" => $total]);
    }


    public function getWorkspace(Request $request, $id) {

        $workspace = Workspace::find(decryptId($id));

        if(!$workspace) {
            abort(422, "You can't perform this action");
        }

        if (! Gate::allows('view-workspace', $workspace)) {
            abort(403, "You can't perform this action");
        }

        return response()->json(['workspace' => $workspace->getData()]);

    }
    
    public function getUsers(Request $request, $id) {

        $request->validate([
            'perPage' => 'required|integer',
            'page' => 'required|integer',

        ]);

        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset = ($request->page - 1) * $perPage;

        $workspace = Workspace::find(decryptId($id));

        if(!$workspace) {
            abort(422, "You can't perform this action");
        }

        $workspacesQuery = $workspace->users();

        if ($request->input("q")) {            
            $workspacesQuery = $workspacesQuery->where(function (Builder $query) use ($request) {
                return $query->where('nickname', 'like' ,"%".$request->q."%")->orWhere('first_name', 'like' ,"%".$request->q."%")->orWhere('last_name', 'like' ,"%".$request->q."%");
            });
        }

        if ($request->input('sortColumn')) {
            if($request->sortColumn == "createdAt"){
                $workspacesQuery = $workspacesQuery->orderBy('joined_at', $request->sort);
            }
            if($request->sortColumn == "name"){
                $workspacesQuery = $workspacesQuery->orderBy('nickname', $request->sort)->orderBy('first_name', $request->sort)->orderBy('last_name', $request->sort)->orderBy('email', $request->sort);
            }
         }
         

        $users = $workspacesQuery->offset($offset)
        ->limit($perPage)
        ->get();
        
        $usersArray = [];

        foreach($users as $user) {
            $usersArray[] = $user->getData();
        }
        
        

        if ($request->input("q")) {
            $total = $workspace->users()->where(function (Builder $query) use ($request) {
                return $query->where('nickname', 'like' ,"%".$request->q."%")->orWhere('first_name', 'like' ,"%".$request->q."%")->orWhere('last_name', 'like' ,"%".$request->q."%");
            })->count();
        } else {
            $total = $workspace->users()->count();
        }

        return response()->json(["users" => $usersArray, "total" => $total, "workspaceName" => $workspace->name]);
    }

    public function inviteUser(Request $request, $id ) {

        $request->validate([
            'email' => 'required|email'
        ]);

        $workspace = Workspace::find(decryptId($id));

        if(!$workspace) {
            abort(422, "You can't perform this action!");
        }

        if (! Gate::allows('update-workspace', $workspace)) {
            abort(422, "You can't perform this action!!");
        }

        $user = User::where("email", $request->email)->first();
        $existingUser = true;
        if (!$user) {
            $user = new User;
            $user->email = $request->email;
            $user->role = "member";
            $user->first_name = $request->firstName ? $request->firstName : "";
            $user->last_name = $request->lastName ? $request->lastName : "";
            
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $password = substr(str_shuffle($permitted_chars), 0, 16);
            $user->password = Hash::make($password);
            $user->save();
            $user->sendEmailVerificationNotification();

            $existingUser = false;

        } else {
            $alreadyExist  = DB::table('user_workspace')
                            ->where('workspace_id', $workspace->id)
		                    ->where('user_id', $user->id)
                     	    ->first();
            if ($alreadyExist) {
                return response()->json(["message" => "User already available in this workspace!"]);
            }
        }

        $workspace->users()->detach($user->id);

        $workspace->users()->attach($user->id, [
            'joined_at' => null,
            'nickname' => $request->nickname ? $request->nickname : null, 
            'created_at' => now(), 
            'updated_at' => now()
        ]);
        
        $workspace->id = decryptId($id);

        $invitationUrl = url("/workspace/{$id}/invitation/{$user->enc_id}");
        
        Mail::to($user->email)
        ->cc(['ronu5879@gmail.com'])
        ->send(new WorkspaceInvitation($user, $invitationUrl, $existingUser, $workspace));

        return response()->json(["message" => "User invited successfully!"]);

    }

    public function workspaceBulkInvite(Request $request) {
        
        $request->validate([
            "invitations"    => "required|array",
            'invitations.*.email' => 'required|email',
            "invitations.*.workspace"    => "required|string",
        ]);

        $totalInvitationSent = 0;
        
        foreach($request->invitations as $invitation) {
            
            $workspace = Workspace::find(decryptId($invitation['workspace']));

            if(!$workspace) {
                continue;
            }

            if (! Gate::allows('update-workspace', $workspace)) {
                continue;
            }

            $user = User::where("email", $invitation['email'])->first();
            $existingUser = true;
            if (!$user) {

                $user = new User;

                $user->email = $invitation['email'];
                $user->role = "member";

                $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $password = substr(str_shuffle($permitted_chars), 0, 16);
                
                $user->password = Hash::make($password);
                $user->save();
                
                $user->sendEmailVerificationNotification();

                $existingUser = false;

            } else {
                $alreadyExist  = DB::table('user_workspace')
                                ->where('workspace_id', $workspace->id)
                                ->where('user_id', $user->id)
                                ->first();
                if ($alreadyExist) {
                    continue;
                }
            }

            $workspace->users()->detach($user->id);

            $workspace->users()->attach($user->id, [
                'joined_at' => null,
                'nickname' => null, 
                'created_at' => now(), 
                'updated_at' => now()
            ]);
            

            $invitationUrl = url("/workspace/{$workspace->enc_id}/invitation/{$user->enc_id}");
            
            Mail::to($user->email)
            ->cc(['ronu5879@gmail.com'])
            ->send(new WorkspaceInvitation($user, $invitationUrl, $existingUser, $workspace));

            $totalInvitationSent++;
        }

        $message = $totalInvitationSent > 1 ? "$totalInvitationSent Invitations sent successfully!" : "$totalInvitationSent Invitation sent successfully!";
        $data['message'] = $message;

        return response()->json($data);

    }


    public function acceptInvitation(Request $request, $id, $memberId) {
        
        $invitee = User::find(decryptId($memberId));
        $workspace = Workspace::find(decryptId($id));

        if(!$invitee || !$workspace) {
            return redirect(config('app.frontend_url')."/?error_message=You can't perform this action!");
        }

        if($invitee->email != Auth::user()->email) {
            return redirect(config('app.frontend_url')."/?error_message=You can't perform this action!");
        }

        $pivot_entry  = DB::table('user_workspace')
                            ->where('workspace_id', decryptId($id))
		                    ->where('user_id', Auth::user()->id)
                     	    ->first();

        if (!$pivot_entry) {
            abort(422, "You can't perform this action!");
        }

        if (strtotime($pivot_entry->created_at) < strtotime ( '-7 day' , time() ) ){
            return redirect(config('app.frontend_url')."/?error_message=Invitation expired!");
        }


        $workspace->users()->updateExistingPivot(Auth::user()->id, [
            'joined_at' => now(),
        ]);

        // return redirect(config('app.frontend_url')."/workspace/$id?success_message=Invitation accepted successfully!");
        return redirect(config('app.frontend_url')."/workspaces?success_message=Invitation accepted successfully!");
    }

    public function deleteMemberFromWorkspace(Request $request, $id) {

        $pivot_entry  = DB::table('user_workspace')
                            ->where('id', decryptId($id))
                     	    ->first();

        if (!$pivot_entry) {
            abort(422, "You can't perform this action!");
        }

        $workspace = Workspace::find($pivot_entry->workspace_id);

        if (!$workspace) {
            abort(422, "You can't perform this action!");
        }

        if (! Gate::allows('update-workspace', $workspace)) {
            abort(422, "You can't perform this action!");
        }

        $message = $pivot_entry->joined_at ? "User removed successfully!" : "Invitation cancelled successfully!";

        DB::table('user_workspace')->where('id', decryptId($id))->delete();

        return response()->json([ "message" => $message ]);
    }

    public function updateMemberInWorkspace(Request $request, $id) {

        $pivot_entry  = DB::table('user_workspace')
                            ->where('id', decryptId($id))
                     	    ->first();

        if (!$pivot_entry) {
            abort(422, "You can't perform this action!");
        }

        $workspace = Workspace::find($pivot_entry->workspace_id);

        if (!$workspace) {
            abort(422, "You can't perform this action!");
        }

        if (! Gate::allows('update-workspace', $workspace)) {
            abort(422, "You can't perform this action!");
        }

        DB::table('user_workspace')->where('id', decryptId($id))->update(['nickname' => $request->nickname]);
        $message = "User updated successfully!";


        return response()->json([ "message" => $message ]);
    }


    public function markWorkspaceAsAccessedNow(Request $request, $id) {

        $workspace = Workspace::find(decryptId($id));

        if (!$workspace) {
            abort(422, "You can't perform this action!");
        }

        if (Auth::user()->role == "company" && Auth::user()->id == $workspace->company_id) {
            
            $workspace->last_accessed_at = now();
            $workspace->save();

            return response()->json(["message" => "Marked workspace as accessed now"]);
        }

        $pivot_entry  = DB::table('user_workspace')
                            ->where('workspace_id', decryptId($id))
		                    ->where('user_id', Auth::user()->id)
                     	    ->first();

        if (!$pivot_entry) {
            abort(422, "You can't perform this action!");
        }
        

        $workspace->users()->updateExistingPivot(Auth::user()->id, [
            'last_accessed_at' => now(),
        ]);

        return response()->json(["message" => "Marked workspace as accessed now"]);

    }

    public function recentlyAccessedWorkspaces() {

        $user = Auth::user();

        if ($user->role == "company") {
            $workspaces = Workspace::where("company_id", $user->id)->orderBy('last_accessed_at', "desc")->limit(10)->get();
        } else {
            $workspaces = $user->workspaces()->orderBy('pivot_last_accessed_at', "desc")->limit(10)->get();
        }

        $workspacesArray = [];

        foreach($workspaces as $workspace) {
            $workspacesArray[] = $workspace->getData();
        }

        return response()->json(['recentWorkspaces' => $workspacesArray]);

    }
}

