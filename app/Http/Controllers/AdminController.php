<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Country;
use App\Models\Feedback;
use App\Models\Notification;
use App\Models\Product;
use App\Models\ProductFeatures;
use App\Models\ProductPrices;
use App\Models\Setting;
use App\Models\Timezone;
use App\Models\User;
use App\Models\Workspace;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class AdminController extends Controller
{
    public function getWorkspaces(Request $request) {
        
        $request->validate([
            'perPage' => 'required|integer',
            'page' => 'required|integer',
            'company' => 'required'

        ]);

        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset = ($request->page - 1) * $perPage;
        
        $workspacesQuery = Workspace::where('company_id', decryptId($request->company))->withCount('users');

        if ($request->input("q")) {
            $workspacesQuery = $workspacesQuery->where('name', 'like' ,"%".$request->q."%");
        }

        if ($request->input('sortColumn')) {

            $sortBy = 'name';
            if($request->sortColumn == "createdAt"){
                $sortBy = 'created_at';
            }
            if($request->sortColumn == "name"){
                $sortBy = 'name';
            }
            if($request->sortColumn == "users"){
                $sortBy = 'users_count';
            }

            $workspacesQuery = $workspacesQuery->orderBy($sortBy, $request->sort);
            
         }
         

        
        $workspaces = $workspacesQuery->offset($offset)
        ->limit($perPage)
        ->get();
        
        
        
        $workspacesArray = [];

        foreach($workspaces as $workspace) {
            $workspacesArray[] = $workspace->getData();
        }
        
        

        if ($request->input("q")) {
            $total = Workspace::where('company_id', decryptId($request->company))->where('name', 'like' ,"%".$request->q."%")->count();
        } else {
            $total = Workspace::where('company_id', decryptId($request->company))->count();
        }

        return response()->json(["workspaces" => $workspacesArray, "total" => $total]);
    }


    // public function getUsersAll(Request $request) {

    //     $request->validate([
    //         'perPage' => 'required|integer',
    //         'page' => 'required|integer',

    //     ]);


    //     $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
    //     $offset = ($request->page - 1) * $perPage;


    //     $usersQuery = User::whereIn("role", ["company","member"]);

    //     if ($request->input("q")) {
    //         $usersQuery = $usersQuery->Where('first_name', 'like' ,"%".$request->q."%")->orWhere('last_name', 'like' ,"%".$request->q."%");
    //     }

    //     if ($request->input('sortColumn')) {
    //         if($request->sortColumn == "createdAt"){
    //             $usersQuery = $usersQuery->orderBy('created_at', $request->sort);
    //         }
    //         if($request->sortColumn == "name"){
    //             $usersQuery = $usersQuery->orderBy('first_name', $request->sort)->orderBy('last_name', $request->sort)->orderBy('email', $request->sort);
    //         }
    //      }
         

    //     $users = $usersQuery->offset($offset)
    //     ->limit($perPage)
    //     ->get();
        
    //     $usersArray = [];

    //     foreach($users as $user) {
    //         $usersArray[] = $user->getData();
    //     }
        
        

    //     if ($request->input("q")) {
    //         $total = User::whereIn("role", ["company","member"])->where('first_name', 'like' ,"%".$request->q."%")->orWhere('last_name', 'like' ,"%".$request->q."%")->count();
    //     } else {
    //         $total = User::whereIn("role", ["company","member"])->count();
    //     }

    //     return response()->json(["users" => $usersArray, "total" => $total]);
    // }
    
    public function getUsers(Request $request) {

        $request->validate([
            'perPage' => 'required|integer',
            'page' => 'required|integer',
            'workspace' => 'required'

        ]);


        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset = ($request->page - 1) * $perPage;

        $workspace = Workspace::find(decryptId($request->workspace));

        if(!$workspace) {
            abort(422, "You can't perform this action");
        }

        $usersQuery = $workspace->users();

        if ($request->input("q")) {
            $usersQuery = $usersQuery->where(function (Builder $query) use ($request) {
                return $query->where('first_name', 'like' ,"%".$request->q."%")->orWhere('last_name', 'like' ,"%".$request->q."%");
            });
        }

        if ($request->input('sortColumn')) {
            if($request->sortColumn == "createdAt"){
                $usersQuery = $usersQuery->orderBy('created_at', $request->sort);
            }
            if($request->sortColumn == "name"){
                $usersQuery = $usersQuery->orderBy('first_name', $request->sort)->orderBy('last_name', $request->sort)->orderBy('email', $request->sort);
            }
         }
        
        $users = $usersQuery->offset($offset)
        ->limit($perPage)
        ->get();
        
        $usersArray = [];

        foreach($users as $user) {
            $usersArray[] = $user->getData();
        }
        
        

        if ($request->input("q")) {
            $total = $workspace->users()->where(function (Builder $query) use ($request) {
                return $query->where('first_name', 'like' ,"%".$request->q."%")->orWhere('last_name', 'like' ,"%".$request->q."%");
            })->count();
        } else {
            $total = $workspace->users()->count();
        }

        return response()->json(["users" => $usersArray, "total" => $total]);
    }
    
    
    public function getCompanies(Request $request) {

        $request->validate([
            'perPage' => 'required|integer',
            'page' => 'required|integer',

        ]);


        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset = ($request->page - 1) * $perPage;


        $usersQuery = User::withCount('companyWorkspaces')->where("role", "company");

        if ($request->input("q")) {
            $usersQuery = $usersQuery->where(function (Builder $query) use ($request) {
                return $query->where('company_name', 'like' ,"%".$request->q."%")->orWhere('first_name', 'like' ,"%".$request->q."%")->orWhere('last_name', 'like' ,"%".$request->q."%");
            });
        
        }

        if ($request->input('sortColumn')) {
            if($request->sortColumn == "createdAt"){
                $usersQuery = $usersQuery->orderBy('created_at', $request->sort);
            }
            if($request->sortColumn == "companyName"){
                $usersQuery = $usersQuery->orderBy('company_name', $request->sort);
            }
            
            if($request->sortColumn == "workspaces"){
                $usersQuery = $usersQuery->orderBy('company_workspaces_count', $request->sort);
            }
         }
         

        $users = $usersQuery->offset($offset)
        ->limit($perPage)
        ->get();
        
        $usersArray = [];

        foreach($users as $user) {
            $usersArray[] = $user->getCompanyData();
        }
        
        

        if ($request->input("q")) {
            $total = User::where("role", "company")->where(function (Builder $query) use ($request) {
                return $query->where('company_name', 'like' ,"%".$request->q."%")->orWhere('first_name', 'like' ,"%".$request->q."%")->orWhere('last_name', 'like' ,"%".$request->q."%");
            })->count();
        } else {
            $total = User::where("role", "company")->count();
        }

        return response()->json(["companies" => $usersArray, "total" => $total]);
    }


    public function getPlans(Request $request) {
        //$plans = Product::with(["features","prices"])->get();

        //return response()->json(["plans" => $plans]);

        $request->validate([
            'perPage' => 'required|integer',
            'page' => 'required|integer',

        ]);


        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset = ($request->page - 1) * $perPage;


        // $plansQuery = Product::;

        if ($request->input("q")) {
            $plansQuery = Product::where(function (Builder $query) use ($request) {
                return $query->where('name', 'like' ,"%".$request->q."%");
            });
        
        } else {
            $plansQuery = false;
        }

        if ($request->input('sortColumn')) {
            if($request->sortColumn == "createdAt"){
                $plansQuery = $plansQuery ? $plansQuery->orderBy('created_at', $request->sort) : Product::orderBy('created_at', $request->sort);
            }
            if($request->sortColumn == "name"){
                $plansQuery = $plansQuery ? $plansQuery->orderBy('name', $request->sort) : Product::orderBy('name', $request->sort);
            }
            
         }
         

        $plans = $plansQuery ? $plansQuery->offset($offset)->limit($perPage)->get() : Product::offset($offset)->limit($perPage)->get();
        
        $plansArray = [];

        foreach($plans as $plan) {
            $plansArray[] = $plan->getData();
        }
        
        

        if ($request->input("q")) {
            $total = Product::where(function (Builder $query) use ($request) {
                return $query->where('name', 'like' ,"%".$request->q."%");
            })->count();
        } else {
            $total = Product::count();
        }

        return response()->json(["plans" => $plansArray, "total" => $total]);
    }


    public function createPlan(Request $request) {
        $request->validate([
            'name' => 'required|max:50|unique:App\Models\Product,name',
            'description' => 'required|max:200',
            'trialDays' => 'sometimes|numeric'
        ]);

        $plan = new Product;
        $plan->name = $request->name;
        $plan->description = $request->description;
        $plan->is_free_plan = $request->isFreePlan ? true : false;

        if ($request->isFreePlan) {
            $plan->trial_days = $request->trialDays ? $request->trialDays : 365;
        }
        
        // Stripe create product and save product id if plan is not free
        if (!$request->isFreePlan) {
            $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
            $stripeProduct = $stripe->products->create(['name' => $plan->name]);
            $plan->product_id = $stripeProduct->id;
        }

        $plan->save();

        return response()->json(["plan" => $plan->getData(), "message" => "Plan created successfully!"]);
    }
    
    public function updatePlan(Request $request, $id) {

        $planId = decryptId($id);
        $request->validate([
            'name' => 'sometimes|max:50|unique:App\Models\Product,name,'.$planId,
            'active' => 'sometimes|boolean',
            'description' => 'sometimes|max:200'
        ]);

        $plan = Product::find($planId);

        if (!$plan) {
            return response()->json(["message" => "Plan not exists!"], 422);
        }

        if ($request->name) {
            $plan->name = $request->name;
        }
        
        if ($request->description) {
            $plan->description = $request->description;
        }
        
        if (isset($request->active)) {
            if ($request->active) {
                $activePrices = ProductPrices::where("product_id", $planId)->where("active", 1)->count();

                if (!$activePrices && !$plan->is_free_plan) {
                    return response()->json(["message" => "Atleast one price needs active!"], 422);
                }
            
                $maxOrderNumber = Product::where('active', 1)->max('order_number');
                if (!$maxOrderNumber) $maxOrderNumber = 0;

                $plan->order_number = $maxOrderNumber + 1;

                if ($plan->is_free_plan) { //deactivate other active free plans
                    Product::where("is_free_plan", true)->where("active", true)->update(['active'=> false]);
                }
            
            }
            $plan->active = $request->active;
            
        }

        $plan->save();

        return response()->json(["plan" => $plan->getData(), "message" => "Plan updated successfully!"]);
    }

    public function getPlanPrices(Request $request, $id) {

        $request->validate([
            'perPage' => 'required|integer',
            'page' => 'required|integer',

        ]);


        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset = ($request->page - 1) * $perPage;

        $pricesQuery = ProductPrices::where("product_id", decryptId($id));
        $product = Product::find(decryptId($id));

        if ($request->input('sortColumn')) {
            if($request->sortColumn == "createdAt"){
                $pricesQuery = $pricesQuery->orderBy('created_at', $request->sort);
            }
            
            if($request->sortColumn == "amount"){
                $pricesQuery = $pricesQuery->orderBy('amount', $request->sort);
            }
        }
         

        $prices = $pricesQuery->offset($offset)->limit($perPage)->get();
        
        $pricesArray = [];

        foreach($prices as $price) {
            $pricesArray[] = $price->getData();
        }
        
        $total = ProductPrices::where("product_id", decryptId($id))->count();
        $isFreePlan = $product->is_free_plan ? true : false;

        return response()->json(["prices" => $pricesArray, "total" => $total, "isFreePlan" => $isFreePlan]);

    }

    public function createPlanPrice(Request $request, $id) {
        $request->validate([
            'cycle' => 'required|in:monthly,yearly',
            'amount' => 'required|numeric|min:1|max:1000'
        ]);

        $plan = Product::find(decryptId($id));
        
        if (!$plan) {
            return response()->json(["message" => "Plan not exists!"], 422);
        }
        
        if ($plan->is_free_plan) {
            return response()->json(["message" => "Free plan can not have prices!"], 422);
        }

        // create prices
        $price = new ProductPrices;
        $price->product_id = $plan->id; 
        $price->cycle = $request->cycle;
        $price->amount = $request->amount;
        $price->price_id = "";
        $price->save(); 

        return response()->json(["price" => $price->getData(), "message" => "Price created successfully!"]);
    }

    public function updatePlanPrice(Request $request, $id) {

        $priceId = decryptId($id);
        $request->validate([
            'active' => 'sometimes|boolean'
        ]);

        $price = ProductPrices::find($priceId);

        if (!$price) {
            return response()->json(["message" => "Price not exists!"], 422);
        }
        
        if (isset($request->active)) {
            $price->active = $request->active;

            if ($price->active) {
                ProductPrices::where("product_id", $price->product_id)->where("cycle", $price->cycle)->update(['active' => 0]);

                // if price_id blank then create price in Stripe
                if (!$price->price_id) {

                    $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
                    $product = Product::find($price->product_id);
                    $interval = $price->cycle == "monthly" ? "month" : "year";

                    $stripePrice = $stripe->prices->create(
                        [
                          'product' => $product->product_id,
                          'unit_amount' => $price->amount*100,
                          'currency' => 'usd',
                          'recurring' => ['interval' => $interval],
                        ]
                    );
                    $price->price_id = $stripePrice->id;
                      
                }
            }
        }

        $price->save();

        $message = $price->active ? "Price activated successfully!" : "Price deactivated successfully!";

        return response()->json(["price" => $price->getData(), "message" => $message]);
    }

    public function createPlanFeature(Request $request, $id) {
        $request->validate([
            'title' => 'required|max:200'
        ]);

        $plan = Product::find(decryptId($id));
        
        if (!$plan) {
            return response()->json(["message" => "Plan not exists!"], 422);
        }

        $maxOrderNumber = ProductFeatures::where('product_id', decryptId($id))->max('order_number');
        if (!$maxOrderNumber) $maxOrderNumber = 0;
        // create prices
        $feature = new ProductFeatures;
        $feature->product_id = $plan->id; 
        $feature->title = $request->title; 
        $feature->order_number = $maxOrderNumber + 1;
        $feature->save(); 

        return response()->json(["feature" => $feature->getData(), "message" => "Feature created successfully!"]);
    }


    public function updatePlanFeature(Request $request, $id) {
        $request->validate([
            'title' => 'required|max:200'
        ]);

        $feature = ProductFeatures::find(decryptId($id));
        
        if (!$feature) {
            return response()->json(["message" => "Feature not exists!"], 422);
        }

        // update feature
        $feature->title = $request->title; 
        $feature->save(); 

        return response()->json(["feature" => $feature->getData(), "message" => "Feature updated successfully!"]);
    }
    
    public function deletePlanFeature(Request $request, $id) {

        $feature = ProductFeatures::find(decryptId($id));
        
        if (!$feature) {
            return response()->json(["message" => "Feature not exists!"], 422);
        }

        $feature->delete(); 

        return response()->json(["message" => "Feature deleted successfully!"]);
    }

    public function getPlanFeatures(Request $request, $id) {
        $productFeatures = ProductFeatures::where('product_id', decryptId($id))->get();

        $productFeaturesArray = [];

        foreach($productFeatures as $feature) {
            $productFeaturesArray[] = $feature->getData();
        }

        return response()->json(['features' => $productFeaturesArray]);
    }


    public function orderPlanFeatures(Request $request, $id) {
        $request->validate([
            "orderedIds"    => "required|array",
            'orderedIds.*' => 'required|string',
        ]);
        $orderNumber = 1;
        foreach($request->orderedIds as $featureId) {
            $feature = ProductFeatures::where("product_id", decryptId($id))->find(decryptId($featureId));

            if ($feature){
                $feature->order_number = $orderNumber;
                $feature->save();
                $orderNumber++;
            }
        }

        return response()->json(["message" => "Features order updated successfully!"]);
    }

    public function getActivePlanDetails() {
        $products = Product::with(["features", "activePrices"])->where("active", 1)->orderBy("order_number")->get();
        
        $productArr = [];

        foreach($products as $product) {
            $productArr[] = $product->getData();
        }

        return response()->json(["plans" => $productArr]);
    }


    public function orderProducts(Request $request) 
    {
        $request->validate([
            "orderedIds"    => "required|array",
            'orderedIds.*' => 'required|string',
        ]);
        $orderNumber = 1;
        foreach($request->orderedIds as $productId) {
            $product = Product::where("active", 1)->find(decryptId($productId));

            if ($product){
                $product->order_number = $orderNumber;
                $product->save();
                $orderNumber++;
            }
        }

        return response()->json(["message" => "Products order updated successfully!"]);
    }

    public function getCountries(Request $request) 
    {
        $request->validate([
            'perPage' => 'required|integer',
            'page'    => 'required|integer',
        ]);

        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset  = ($request->page - 1) * $perPage;

        /* to initiate country model */
        $countries_list = Country::query();

        if ($request->input("q")) {
            $countries_list = $countries_list->where('name','like', "%".$request->q."%");    
        } 

        $total = $countries_list->count();

        if ($request->sortColumn == "name") {
            $countries_list = $countries_list->orderBy('name', $request->sort);
        }

        if ($request->sortColumn == "createdAt") {
            $countries_list = $countries_list->orderBy('created_at', $request->sort);
        }
        
        $countries_list = $countries_list->offset($offset)->limit($perPage)->get();

        $countriesArray = [];

        foreach($countries_list as $country) {
            $countriesArray[] = $country->getData();
        }
        
        return response()->json(["countries" => $countriesArray, "total" => $total]);
    }

    public function updateCountry(Request $request, $id) 
    {
        $timezone_id = decryptId($id);

        $request->validate([
            'country_code' => 'required|string|exists:countries,code',
            'timezone'     => 'required|string'
        ]);

        $timezone = Timezone::find($timezone_id);

        if (!$timezone) {
            return response()->json(["message" => "Timezone not exists!"], 422);
        }

        $country_id = Country::where('code', $request->country_code)->first()->id;

        $timezone_ins =  $timezone->updateOrCreate(['id'=>$timezone_id], [
            'country_id' => $country_id,
            'timezone'   => $request->timezone,
            'gmt_offset' => $request->gmt_offset ?? null
        ]);
        
        $timezone_ins->load('country');

        return response()->json(["timezone" => $timezone_ins->getData(), "message" => "Timezone updated successfully!"]);
    }

    public function getTimezones(Request $request) 
    {
        $request->validate([
            'perPage' => 'required|integer',
            'page'    => 'required|integer',
        ]);

        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset  = ($request->page - 1) * $perPage;

        /* to initiate timezone model */
        $timezone_list = Timezone::query();

        if ($request->input("q")) {
            $timezone_list = Timezone::where('name','like', "%".$request->q."%");    
        }

        $total = $timezone_list->count();

        if ($request->sortColumn == "createdAt") {
            $timezone_list = $timezone_list->orderBy('created_at', $request->sort);
        }
        
        if ($request->sortColumn == "name") {
            $timezone_list = $timezone_list->orderBy('name', $request->sort);
        }

        $timezone_list = $timezone_list->offset($offset)->limit($perPage)->get();

        $timezonesArray = [];

        foreach($timezone_list as $timezone) {
            $timezonesArray[] = $timezone->getData();
        }
        
        return response()->json(["timezones" => $timezonesArray, "total" => $total]);
    }

    public function createTimezone(Request $request) 
    {
        $validated = $request->validate([
            'name'       => 'required|string',
            'identifier' => 'required|string',
            'group'      => 'required|string'
        ]);

        $timezone = Timezone::create($validated);

        return response()->json(["timezone" => $timezone->getData(), "message" => "Timezone created successfully!"]);
    }

    public function updateTimezone(Request $request, $id) 
    {
        $timezone_id = decryptId($id);

        $validated = $request->validate([
            'name'       => 'required|string',
            'identifier' => 'required|string',
            'group'      => 'required|string'
        ]);

        $timezone = Timezone::find($timezone_id);

        if (!$timezone) {
            return response()->json(["message" => "Timezone not exists!"], 422);
        }

        $timezone_ins =  $timezone->updateOrCreate(['id'=>$timezone_id], $validated);

        return response()->json(["timezone" => $timezone_ins->getData(), "message" => "Timezone updated successfully!"]);
    }

    public function getBookings(Request $request) 
    {
        $request->validate([
            'perPage' => 'required|integer',
            'page'    => 'required|integer',
        ]);

        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset  = ($request->page - 1) * $perPage;

        $booking_list = Booking::with(['ownerTimezone', 'recipientTimezone']);

        if ($request->input("q")) {
            $booking_list = Booking::where('name','like', "%".$request->q."%");    
        }

        $total = $booking_list->count();

        if ($request->sortColumn == "createdAt") {
            $booking_list = $booking_list->orderBy('created_at', $request->sort);
        }
        
        if ($request->sortColumn == "name") {
            $booking_list = $booking_list->orderBy('name', $request->sort);
        }

        $booking_list = $booking_list->offset($offset)->limit($perPage)->get();

        $bookingsArray = [];

        foreach($booking_list as $booking) {
            $bookingsArray[] = $booking->getData();
        }
        
        return response()->json(["bookings" => $bookingsArray, "total" => $total]);
    }

    public function createBooking(Request $request) 
    {
        $request->validate([
            'name'                  => 'required',
            'owner_timezone_id'     => 'required',
            'recipient_timezone_id' => 'required',
            'start_time'            => 'required',
            'end_time'              => 'required'
        ]);

        $owner_timezone = Timezone::find($request->owner_timezone_id)->identifier;

        $start = strtotime($request->start_time);
        $end   = strtotime($request->end_time);

        $start_time = (new \DateTime(date('Y-m-d H:i:s', $start), new \DateTimeZone($owner_timezone)))->setTimezone(new \DateTimeZone('UTC'));
        $end_time   = (new \DateTime(date('Y-m-d H:i:s', $end), new \DateTimeZone($owner_timezone)))->setTimezone(new \DateTimeZone('UTC'));

        $booking = Booking::create([
            'name'                  => $request->name,
            'owner_timezone_id'     => $request->owner_timezone_id,
            'recipient_timezone_id' => $request->recipient_timezone_id,
            'start_time'            => $start_time->format('Y-m-d H:i:s'),
            'end_time'              => $end_time->format('Y-m-d H:i:s')
        ]);

        $booking->load('ownerTimezone', 'recipientTimezone');

        return response()->json(["booking" => $booking->getData(), "message" => "Booking created successfully!"]);
    }

    public function updateBooking(Request $request, $id) 
    {
        $booking_id = decryptId($id);

        $request->validate([
            'name'                  => 'required',
            'owner_timezone_id'     => 'required',
            'recipient_timezone_id' => 'required',
            'start_time'            => 'required',
            'end_time'              => 'required'
        ]);

        $booking = Booking::find($booking_id);

        if (!$booking) {
            return response()->json(["message" => "Booking not exists!"], 422);
        }

        $owner_timezone = Timezone::find($request->owner_timezone_id)->identifier;

        $start = strtotime($request->start_time);
        $end   = strtotime($request->end_time);

        $start_time = (new \DateTime(date('Y-m-d H:i:s', $start), new \DateTimeZone($owner_timezone)))->setTimezone(new \DateTimeZone('UTC'));
        $end_time   = (new \DateTime(date('Y-m-d H:i:s', $end), new \DateTimeZone($owner_timezone)))->setTimezone(new \DateTimeZone('UTC'));

        $booking_ins = Booking::updateOrCreate(['id'=>$booking_id], [
            'name'                  => $request->name,
            'owner_timezone_id'     => $request->owner_timezone_id,
            'recipient_timezone_id' => $request->recipient_timezone_id,
            'start_time'            => $start_time->format('Y-m-d H:i:s'),
            'end_time'              => $end_time->format('Y-m-d H:i:s')
        ]);

        $booking_ins->load('ownerTimezone', 'recipientTimezone');

        return response()->json(["booking" => $booking_ins->getData(), "message" => "Timezone updated successfully!"]);
    }

    public function getFeedbacks(Request $request) 
    {
        $request->validate([
            'perPage' => 'required|integer',
            'page'    => 'required|integer',
        ]);

        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset  = ($request->page - 1) * $perPage;

        /* to initiate timezone model */
        $feedback_list = Feedback::query();

        if ($request->input("q")) {
            // $feedback_list = Feedback::whereRelation('user', 'first_name', 'like', "%".$request->q."%");    
            $feedback_list = Feedback::whereRelation('user', function ($query) use ($request) {
                $query->where('first_name', 'like', "%".$request->q."%")->orWhere('last_name', 'like', "%".$request->q."%");
            });  
        }

        $total = $feedback_list->count();

        if ($request->sortColumn == "createdAt") {
            $feedback_list = $feedback_list->orderBy('created_at', $request->sort);
        }
        
        if ($request->sortColumn == "rating") {
            $feedback_list = $feedback_list->orderBy('rating', $request->sort);
        }

        $feedback_list = $feedback_list->offset($offset)->limit($perPage)->get();

        $feedbacksArray = [];

        foreach($feedback_list as $feedback) {
            $feedbacksArray[] = $feedback->getData();
        }
        
        return response()->json(["feedbacks" => $feedbacksArray, "total" => $total]);
    }

    public function getSettings() 
    {
        $setting_list = Setting::all();

        $settingsArray = [];

        foreach($setting_list as $setting) {
            $settingsArray[] = $setting->getData();
        }
        
        return response()->json(["settings" => $settingsArray]);
    }

    public function updateSetting(Request $request) 
    {
        $data = $request->all();

        foreach ($data as $value) {
            $id      = decryptId($value['id']);
            $setting = Setting::find($id);

            if (!$setting) {
                return response()->json(["message" => "Settings not exists!"], 422);
            }

            $setting->updateOrCreate(['id'=>$id], [
                'value'   => $value['value'],
                'message' => $value['message']
            ]);
        }

        return response()->json(["setting" => [], "message" => "Settings updated successfully!"]);
    }

    public function getNotifications(Request $request) 
    {
        $request->validate([
            'perPage' => 'required|integer',
            'page'    => 'required|integer',
        ]);

        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset  = ($request->page - 1) * $perPage;

        /* to initiate timezone model */
        $notification_list = Notification::query();

        if ($request->input("q")) {
            $notification_list = Notification::whereRelation('user', function ($query) use ($request) {
                $query->where('first_name', 'like', "%".$request->q."%")->orWhere('last_name', 'like', "%".$request->q."%");
            });    
        }

        $total = $notification_list->count();

        if ($request->sortColumn == "readAt") {
            $notification_list = $notification_list->orderBy('read_at', $request->sort);
        }

        if ($request->sortColumn == "createdAt") {
            $notification_list = $notification_list->orderBy('created_at', $request->sort);
        }

        $notification_list = $notification_list->offset($offset)->limit($perPage)->get();

        $notificationsArray = [];

        foreach($notification_list as $timezone) {
            $notificationsArray[] = $timezone->getData();
        }
        
        return response()->json(["notifications" => $notificationsArray, "total" => $total]);
    }

    public function createNotification(Request $request) 
    {
        $request->validate([
            'type'     => 'required',
            'message'  => 'required|string'
        ]);

        $members_list = User::select('id')->whereIn('role', ['member', 'company'])->get();

        foreach ($members_list as $member) {
            Notification::create([
                'user_id' => $member->id,
                'type'    => $request->type ?? 1,
                'message' => $request->message ?? null
            ]);
        }

        return response()->json(["message" => "Site warning created successfully!"]);
    }

    public function updateNotification(Request $request, $id) 
    {
        $notification_id = decryptId($id);

        $request->validate([
            'user_id'  => 'required',
            'type'     => 'required',
            'message'  => 'required|string'
        ]);

        $notification = Notification::find($notification_id);

        if (!$notification) {
            return response()->json(["message" => "Site warning not exists!"], 422);
        }

        $query = $notification->updateOrCreate(['id'=>$notification_id], [
            'user_id' => $request->user_id,
            'type'    => $request->type ?? 1,
            'message' => $request->message
        ]);

        return response()->json(["notification" => $query->getData(), "message" => "Site warning updated successfully!"]);
    }

    public function getMembersList() 
    {
        $members_list = User::whereIn('role', ['member', 'company'])->whereNotNull('profile_completed_at')->get();

        $membersArray = [];

        foreach($members_list as $member) {
            $membersArray[] = $member->getMemberDropdown();
        }

        return response()->json(["members_list" => $membersArray]);
    }

    /* For Later Booking Moudle */
    // $request->validate([
    //     'user_id'    => 'required',
    //     'duration'   => 'required',
    //     'start_time' => 'required',
    //     'end_time'   => 'required'
    // ]);

    // $user = User::where('id', $request->user_id)->with('timezone:id,timezone')->first();
    // /* Later we will select timezone from country if user not set the timezone */
    // if (is_null($user->timezone)) {
    //     $user_tz = $user->region->country->timezone[0]->timezone ?? 'UTC';
    // } else {
    //     $user_tz = $user->timezone->timezone;
    // }

    // $start = strtotime($request->start_time);
    // $end   = strtotime($request->end_time);

    // $start_time = new \DateTime(date('Y-m-d H:i:s', $start), new \DateTimeZone($user_tz));
    // $end_time   = new \DateTime(date('Y-m-d H:i:s', $end), new \DateTimeZone($user_tz));

    // $booking = Booking::create([
    //     'user_id'    => $request->user_id,
    //     'duration'   => $request->duration,
    //     'start_time' => $start_time,
    //     'end_time'   => $end_time
    // ]);

    // return $booking;
    // dd($start_time->format('Y-m-d H:i:s'), $end_time->format('Y-m-d H:i:s'));

    // $datetime_khi = '2022-10-10 10:45:00';

    // $date = new \DateTime($datetime_khi, new \DateTimeZone('Europe/London')); // USER's timezone
    // $formmm = $date->format('Y-m-d H:i:s');
    // $date->setTimezone(new \DateTimeZone('UTC'));
    // dd($formmm);
}