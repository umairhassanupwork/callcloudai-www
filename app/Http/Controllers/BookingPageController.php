<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Workspace;
use App\Models\BookingPage;
use App\Models\User;
use Auth;
use DB;

// use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Gate;
// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Mail;
// use Illuminate\Database\Eloquent\Builder;

// use App\Mail\WorkspaceInvitation;

class BookingPageController extends Controller
{
    public function create(Request $request) {

        if (!$request->workspace_id) {
            abort(422, "Workspace is required");
        }
        
        $request->validate([
            'title' => 'required|unique:App\Models\BookingPage,title,NULL,id,workspace_id,'.$request->workspace_id,
            'intro' => 'required|string',
            'workspace_id' => 'required|string',
            'slug' => 'required|unique:App\Models\BookingPage,slug',
            'logo' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);

        $workspace = Workspace::find(decryptId($request->workspace_id));

        if (!$workspace || !Gate::allows('create-booking-page', $workspace)) {
            abort(403, "You can't perform this action");
        }

        $bookingPage = new BookingPage;
        $bookingPage->title = $request->title;
        $bookingPage->intro = $request->intro;
        $bookingPage->slug = $request->slug;
        $bookingPage->workspace_id = $workspace->id;
        $bookingPage->created_by = Auth::user()->id;

        if($request->file('logo')){
            $path = $request->file('logo')->store('booking_page_logo','public');
            $bookingPage->logo = config('app.url').'/storage/'.$path;
        }

        $bookingPage->save();

        $data['message'] = 'Booking page created successfully!';
        $data['bookingPage'] = $bookingPage->getData();

        return response()->json($data);
    }


    public function getBookingPages(Request $request) {

        $request->validate([
            'perPage' => 'required|integer',
            'page' => 'required|integer',
            'workspace_id' => 'required|string'

        ]);

        $perPage = $request->perPage && $request->perPage < 100 ? $request->perPage : 100;
        $offset = ($request->page - 1) * $perPage;

        $user = Auth::user();
        $workspace = Workspace::find(decryptId($request->workspace_id));

        if (!$workspace || !Gate::allows('view-workspace', $workspace)) {
            abort(403, "You can't perform this action");
        }

        $bookingPagesQuery = BookingPage::where("workspace_id", $workspace->id);
        if ($request->input("q")) {
            $bookingPagesQuery = $bookingPagesQuery->where('title', 'like' ,"%".$request->q."%");
        }

        if ($request->input('sortColumn')) {
            if($request->sortColumn == "createdAt"){
                $bookingPagesQuery = $bookingPagesQuery->orderBy('created_at', $request->sort);
            }
        }

        $bookingPages = $bookingPagesQuery->offset($offset)
            ->limit($perPage)
            ->get();
            
        $bookingPagesArray = [];

        foreach($bookingPages as $bookingPage) {
            $bookingPagesArray[] = $bookingPage->getData();
        }
        
        if ($request->input("q")) {
            $total = BookingPage::where("workspace_id", $workspace->id)->where('title', 'like' ,"%".$request->q."%")->count();
        } else {
            $total = BookingPage::where("workspace_id", $workspace->id)->count();
        }
        
        return response()->json(["bookingPages" => $bookingPagesArray, "total" => $total]);
    }


    public function deleteBookingPage(Request $request, $id) {

        $user = Auth::user();
        $bookingPage = BookingPage::find(decryptId($id));

        if (!$bookingPage) {
            abort(403, "You can't perform this action");
        }

        $workspace = Workspace::find($bookingPage->workspace_id);

        if (!$workspace || !Gate::allows('delete-booking-page', $workspace, $bookingPage)) {
            abort(403, "You can't perform this action");
        }

        $bookingPage->delete();

        $data['message'] = 'Booking page deleted successfully!';
        return response()->json($data);

    }
}
