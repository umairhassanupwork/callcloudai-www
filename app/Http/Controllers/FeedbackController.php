<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function create(Request $request) 
    {
        $request->validate([
            'user_id' => 'required',
            'rating'  => 'required|integer',
            'message' => 'required|string'
        ]);

        $feedback = Feedback::create([
            'user_id' => decryptId($request->user_id),
            'rating'  => $request->rating,
            'message' => $request->message
        ]);

        return response()->json(["feedback" => [], "message" => "Feedback submitted. Thanks!"]);   
    }
}
