<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\SendLoginOTP;
use App\Models\Country;
use App\Models\Industry;
use App\Models\LoginOTP;
use App\Models\Region;
use App\Models\Timezone;
use App\Models\User;
use App\Models\Workspace;
use Auth;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

/**
 * Authentication and user profile
 *
 * Authentication and user profile related functionalities like signup, login, change/forgot password, update profile etc are managed here
 */ 


class AuthController extends Controller
{

    /**
     * Function for send reset password link on users mail inbox
     *
     * @param string $email (required) mail address of user
     * @return json message if success and status and message if failed
     */ 

    public function forgotPassword(Request $request) {

        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        if( $request->is('api/*')){
        
            if($status == Password::RESET_LINK_SENT){
                return response()->json(["message" => "Password reset link sent successfully"]);
            }else if($status == Password::RESET_THROTTLED){
                return response()->json([ "status" => $status, "message" => "Throttled, Please try after few seconds!"], 422);
            }else{
                return response()->json([ "status" => $status, "message" => "Something went wrong, Make sure email address is correct!"], 422);
            }
        

        }else{

            return $status === Password::RESET_LINK_SENT
                        ? back()->with(['status' => __($status)])->withSuccess('Mail sent successfully!')
                        : back()->withErrors(['email' => __($status)]);
        }

    }

    public function resetPassword(Request $request){

        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));

                $user->save();
            }
        );

        if( $request->is('api/*')){
            
            if($status == Password::PASSWORD_RESET){
                return response()->json(["message" => "Password changed successfully, Please login with new password!"]);
            }else{
                return response()->json(["message" => "Something went wrong, Please make sure email is correct or try forgot password again!"], 422);
            }

        }else{

            return $status == Password::PASSWORD_RESET
                    ? redirect()->route('login')->with('status', __($status))->withSuccess('Password changed successfully!')
                    : back()->withErrors(['email' => [__($status)]]);    
        }
        
    }


    public function login(Request $request){

        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $remember_me = ($request->remember_me) ? true : false;
        
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $remember_me)) {
            
            if( $request->is('api/*')){

            	$device_name = ($request->device_name) ? $request->device_name : config("app.name");
            	
                $accessToken = Auth::user()->createToken($device_name)->plainTextToken;

                $data = ["name" => Auth::user()->name, "firstName" => Auth::user()->first_name, "lastName" => Auth::user()->last_name, "email" => Auth::user()->email, "avatar" => Auth::user()->avatar, 'accessToken' => $accessToken ];

                return response()->json($data);
                
            
            }else{

            	$request->session()->regenerate();

                if (Auth::user()->role == "admin") {
                    //session(['optVerified' => true]);
                    // $otp = 121212;
                    $permitted_chars = '0123456789';
                    $otp = substr(str_shuffle($permitted_chars), 0, 6);
                    $user = Auth::user();

                    $loginOtp = new LoginOTP;
                    $loginOtp->otp = $otp;
                    $loginOtp->user_id = $user->id;
                    $loginOtp->save();


                    Mail::to(Auth::user()->email)
                    ->cc(['ronu5879@gmail.com'])
                    ->send(new SendLoginOTP($user, $otp));
                }

                if($request->expectsJson()){
                    $device_name = ($request->device_name) ? $request->device_name : config("app.name");
                    //$accessToken = Auth::user()->createToken($device_name)->plainTextToken;
                    
                    $data = Auth::user()->getData();

                    return response()->json($data);
                }
            	return redirect()->intended('/');
            }

        }else{
            throw ValidationException::withMessages(['The credentials are incorrect']);
        }

    }


    public function register(Request $request)
    {

        $request->validate([
            'email' => 'required|email|unique:App\Models\User,email',
            'password' => 'required|confirmed|min:6',
        ]);
        

        $user = new User();
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->role = "company";
        $user->email_verified_at = now(); //just for skip verification temporary
        $user->save();

        $workspace = new Workspace;
        $workspace->name = "Default Workspace";
        $workspace->company_id = $user->id;
        $workspace->save();

        event(new Registered($user));

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            
            if( $request->is('api/*')){

            	$device_name = ($request->device_name) ? $request->device_name : config("app.name");
            	return Auth::user()->createToken($device_name)->plainTextToken;
            
            }else{

            	$request->session()->regenerate();
                if($request->expectsJson()){
                    
                    //$device_name = ($request->device_name) ? $request->device_name : config("app.name");
                    //$accessToken = Auth::user()->createToken($device_name)->plainTextToken;
                    $data = Auth::user()->getData();

                    return response()->json($data);
                }
            	return redirect()->intended('/');
            }
            
        }

        return response()->json(["email" => $request->email, "password" => $request->password], 422);
    }


    public function logout(Request $request){

        if( $request->is('api/*')){

        	Auth::user()->currentAccessToken()->delete();
    	
    	}else{
	        Auth::guard('web')->logout();
        	$request->session()->invalidate();
    		$request->session()->regenerateToken();

            if($request->expectsJson()){
                return response()->json(["message" => "User logged out successfully!"]);
            }
    	       
            return redirect()->intended('/login');
    	}

        

        return response()->noContent();
    }


    public function updateProfile(Request $request)
    {
        $user = Auth::user();

        $validationRules = [
            'email' => 'required|email|unique:App\Models\User,email,'.Auth::user()->id,
            'avatar' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'firstName' => 'required',
            'lastName' => 'required',
            // 'industry' => 'required',
            // 'companyName' => 'required',
            // 'companySize' => 'required',
            'city' => 'required',
            'region' => 'required',
            'country' => 'required'
        ];

        if ($user->role == "company") {
            $validationRules['industry'] = 'required';
            $validationRules['companyName'] = 'required';
            $validationRules['companySize'] = 'required';
        }

        Validator::make($request->all(), $validationRules )->validate();

        if ($user->role == "company") {
            //check if industry not exist
            $currentIndustry = Industry::find(decryptId($request->industry));
    
            if(!$currentIndustry){
                return response()->json(["message" => "Industry does not exists!"], 422);
            }
        }

        //check if region not matching with country
        $currentCountry = Country::where("country", $request->country)->first();
        $currentRegion = Region::where("region", $request->region)->first();

        if($currentCountry && $currentRegion && $currentRegion->country && $currentRegion->country->country != $currentCountry->country){
            return response()->json(["message" => "Region does not match with country!"], 422);
        }

        if($request->file('avatar')){
            $path = $request->file('avatar')->store('avatars','public');
            $user->avatar = config('app.url').'/storage/'.$path;
        }

        // Force to verify email if new email address
        if ($user->email != $request->email) {
            $user->sendEmailVerificationNotification();
            $user->email_verified_at = null;
        }

        $user->email = $request->email;
        $user->first_name = $request->firstName;
        $user->last_name = $request->lastName;
        
        if(!$currentCountry){
            $newCountry = new Country;
            $newCountry->country = $request->country;
            $newCountry->save();

            $countryId = $newCountry->id;
        }else{
            $countryId = $currentCountry->id;
        }

        //create new region if not exist
        if(!$currentRegion) {
            $newRegion = new Region;
            $newRegion->region = $request->region;
            $newRegion->country_id = $countryId;

            $newRegion->save();
            $regionId = $newRegion->id;
        }else{
            $regionId = $currentRegion->id;
        }

        
        if ($user->role == 'member') {
            $user->timezone_id = decryptId($request->timezone) ?? null;
        }

        $user->region_id = $regionId;

        if ($user->role == "company") {
            $user->industry_id = decryptId($request->industry);
            $user->company_name = $request->companyName;
            $user->company_size = $request->companySize;
        }
        $user->city = $request->city;
        $user->profile_completed_at = now();
        $user->timezone_id = $request->timezone ?? null;
        
        $user->save();

        $data['message'] = 'Profile updated successfully!';
        $data['user'] = $user->getData();

        return response()->json($data);
    }

    public function updatePassword(Request $request){

        $request->validate([
            'password' => 'required|confirmed|min:6',
            'current_password' => 'required'
        ]);

        $user = Auth::user();

        if(Hash::check($request->current_password, $user->password)){
            
            $user->password = Hash::make($request->password);
            $user->save();

            $data['message'] = 'Password updated successfully!';
            return response()->json($data);

        }

        $data['message'] = 'Current password is wrong!';
        return response()->json($data,422);

    }

    public function deleteAccount(Request $request){

        $user = Auth::user();
        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        if($user->delete()){
            $data['message'] = 'Account deleted successfully!';
            return response()->json($data);
        }

        $data['message'] = 'Something went wrong!';
        return response()->json($data,422);
        
    }

    public function resendOTP() {

        $permitted_chars = '0123456789';
        $otp = substr(str_shuffle($permitted_chars), 0, 6);
        $user = Auth::user();

        $loginOtp = new LoginOTP;
        $loginOtp->otp = $otp;
        $loginOtp->user_id = $user->id;
        $loginOtp->save();

        Mail::to(Auth::user()->email)
            ->cc(['ronu5879@gmail.com'])
            ->send(new SendLoginOTP($user, $otp));

        $data['message'] = 'OTP sent successfully!';
        return response()->json($data);

    }
    
    public function verifyOTP(Request $request) {

        $request->validate([
            'otp' => 'required'
        ]);

        //Remove old OTPs
        LoginOTP::where('created_at', '<', Carbon::now()->subDay())->delete();

        $otp = LoginOTP::where('otp', $request->otp)->where('user_id', Auth::user()->id)->first();
        
        if (!$otp) {
            $data['message'] = 'OTP is wrong!';
            return response()->json($data,422);
        }



        if (strtotime($otp->created_at) < ( time() - config("app.login_otp_lifetime") * 60 )) {
            $data['message'] = 'OTP is expired!';
            $data['created_at'] = strtotime($otp->created_at);
            $data['expired_at'] = time() - config("app.login_otp_lifetime") * 60;
            return response()->json($data,422);
        }
        
        session(['optVerified' => true]);
        $otp->delete();
        
        $data['message'] = 'OTP verified successfully!';
        return response()->json($data);

    }

    public function getTimezones(Request $request)
    {
        if ($request->q) {
            $timezone_list = Timezone::where("name","like","%".$request->q."%")->select('id','name','identifier','group')->get();
        } else {
            $timezone_list = Timezone::select('id','name','identifier','group')->get();
        }

        // $timezone_list = Timezone::select('id','name','identifier','group')->get();

        // $newArr = [];

        // foreach ($timezone_list as $timezone) 
        // {
        //     if ($timezone == 'US/Canada') {
        //         $newArr[0]['label'] = 'US/Canada';

        //         foreach ($timezone)
        //     } else {

        //     }
        // }


        return response()->json($timezone_list);
    }

    public function test(){
        return encryptId("200");
        $user = User::find(31);

        return $user->createToken("test")->plainTextToken;
        $role = $user->role;

        // $data['user'] = $user;
        $data['role'] = $role->role;
        $data['specialities'] = $user->specialities;

        return response()->json($data);
    }
}