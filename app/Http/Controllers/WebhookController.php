<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductPrices;
use App\Models\SetupIntent;
use App\Models\Invoice;
use Auth;
use Illuminate\Support\Facades\Mail;

use App\Mail\SubscriptionActivated;
use App\Mail\SubscriptionCanceled;



class WebhookController extends Controller
{

    public function handleSetupIntentSucceeded($setupIntent) {

        
        $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));

        $data = null;
        $user = User::where("stripe_customer_id", $setupIntent->customer)->first();

        $paymentMethod = $stripe->paymentMethods->retrieve(
            $setupIntent->payment_method,
            []
        );
            
        if ($user && !$user->stripe_subscription_id) {
            if ($paymentMethod->metadata && $paymentMethod->metadata->action == "subscribe") {
                
                $price = ProductPrices::find(decryptId($paymentMethod->metadata->price));

                if ($price) {

                    $subscription = $stripe->subscriptions->create(
                        [
                            'customer' => $user->stripe_customer_id,
                            'default_payment_method' => $paymentMethod->id,
                            'items' => [['price' => $price->price_id]]
                        ]
                    );
            
                    $user->stripe_subscription_id = $subscription->id;
                    $user->save();

                    // Mail::to($user->email)
                    //     ->cc(['ronu5879@gmail.com'])
                    //     ->send(new SubscriptionCreated($user));

                    $data = [
                        "action" => "Subscription created"
                    ];
                }
            }
        }

        if ($user && $user->stripe_subscription_id && $paymentMethod->metadata && $paymentMethod->metadata->action == "addPaymentMethod" && $paymentMethod->metadata->makeDefault == "true" ) {

            if ($user->stripe_subscription_id) {

                $stripe->subscriptions->update(
                    $user->stripe_subscription_id,
                    ['default_payment_method' => $paymentMethod->id]
                );
        
                $data = [
                    "action" => "Default payment method set"
                ];
            }
        }

        return $data;

    }


    public function handleSubscriptionUpdated($subscription) {
        $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));

        $data = null;
        $user = User::where("stripe_customer_id", $subscription->customer)->first();

        if ($subscription->status == "active") {
            
            try {
                $priceId = $subscription->items->data[0]->price->id;
                $price = ProductPrices::where("price_id", $priceId)->first();

                if ($price) {
                    $user->subscription_price_id = $price->id;
                    $user->subscription_product_id = $price->product_id;
                    $user->subscribed_at = now();
                    $user->stripe_subscription_status = $subscription->status;
                    $user->is_subscribed = 1;
                    $user->save();
        
                    Mail::to($user->email)
                        ->cc(['ronu5879@gmail.com'])
                        ->send(new SubscriptionActivated($user));
        
                    $data = [
                        "action" => "Subscription activated"
                    ];
                }
            } catch (\Exception $e) {
                $data = [
                    "action" => null,
                    "error" => $e->getMessage()
                ];
            }
        } else if ($subscription->status == "canceled") {
            try {

                $user->stripe_subscription_status = $subscription->status;
                $user->save();

                Mail::to($user->email)
                        ->cc(['ronu5879@gmail.com'])
                        ->send(new SubscriptionCanceled($user));

                $data = [
                    "action" => "Subscription canceled"
                ];
                
            } catch (\Exception $e) {
                $data = [
                    "action" => null,
                    "error" => $e->getMessage()
                ];
            }
        } else {

            if (in_array($subscription->status, ['past_due', 'unpaid', 'incomplete', 'incomplete_expired', 'trialing'])) {

                try {
                    $priceId = $subscription->items->data[0]->price->id;
                    $price = ProductPrices::where("price_id", $priceId)->first();
    
                    if ($price) {
                        $user->subscription_price_id = $price->id;
                        $user->subscription_product_id = $price->product_id;
                        $user->subscribed_at = now();
                        $user->stripe_subscription_status = $subscription->status;
                        $user->is_subscribed = 1;
                        $user->save();
            
                        // Mail::to($user->email)
                        //     ->cc(['ronu5879@gmail.com'])
                        //     ->send(new SubscriptionActivated($user));
            
                        $data = [
                            "action" => "Subscription updated"
                        ];
                    }
                } catch (\Exception $e) {
                    $data = [
                        "action" => null,
                        "error" => $e->getMessage()
                    ];
                }

            }
        }

        return $data;
    }
    
    
    public function handleSubscriptionDeleted($subscription) {

        $data = null;
        $user = User::where("stripe_customer_id", $subscription->customer)->first();

        try {

            $user->stripe_subscription_status = "canceled";
            $user->save();

            Mail::to($user->email)
                        ->cc(['ronu5879@gmail.com'])
                        ->send(new SubscriptionCanceled($user));

            $data = [
                "action" => "Subscription canceled"
            ];
            
        } catch (\Exception $e) {
            $data = [
                "action" => null,
                "error" => $e->getMessage()
            ];
        }
 

        return $data;
    }

    public function handleInvoiceCreated($invoice) {
        
        $data = null;
        $user = User::where("stripe_customer_id", $invoice->customer)->first();

        if ($user) {

            $invoiceModel = new Invoice;
            $invoiceModel->user_id = $user->id;
            $invoiceModel->json_data = json_encode($invoice);
            $invoiceModel->stripe_invoice_id = $invoice->id;
            $invoiceModel->amount = $invoice->total/100;
            $invoiceModel->status = $invoice->status;
            $invoiceModel->stripe_payment_intent = $invoice->payment_intent;
            $invoiceModel->save();

            $data = [
                "action" => "invoice created"
            ];
        }

        return $data;
        
    }
    
    public function handleInvoicePaid($invoice) {
        
        $data = null;
        $user = User::where("stripe_customer_id", $invoice->customer)->first();
        $invoiceModel = Invoice::where("user_id", $user->id)->where("stripe_invoice_id", $invoice->id)->first();

        if ($user) {
            
            $invoiceModel->json_data = json_encode($invoice);
            $invoiceModel->amount = $invoice->total/100;
            $invoiceModel->status = $invoice->status;
            $invoiceModel->stripe_payment_intent = $invoice->payment_intent;
            $invoiceModel->stripe_payment_status = "paid";
            $invoiceModel->save();

            $data = [
                "action" => "invoice paid"
            ];
        }

        return $data;
        
    }
    
    public function handleInvoiceDeleted($invoice) {
        
        $data = null;
        $user = User::where("stripe_customer_id", $invoice->customer)->first();
        $invoiceModel = Invoice::where("user_id", $user->id)->where("stripe_invoice_id", $invoice->id)->first();

        if ($invoiceModel) {
            $invoiceModel->delete();
            $data = [
                "action" => "invoice deleted"
            ];
        }

        return $data;
        
    }
    
    
    public function handleInvoicePaymentActionRequired($invoice) {
        
        $data = null;
        $user = User::where("stripe_customer_id", $invoice->customer)->first();
        $invoiceModel = Invoice::where("user_id", $user->id)->where("stripe_invoice_id", $invoice->id)->first();

        if ($invoiceModel) {
            $invoiceModel->stripe_payment_status = "payment_action_required";
            $invoiceModel->json_data = json_encode($invoice);
            $invoiceModel->save();
            $data = [
                "action" => "invoice payment action required"
            ];
        }

        return $data;
        
    }
    
    public function handleInvoicePaymentFailed($invoice) {
        
        $data = null;
        $user = User::where("stripe_customer_id", $invoice->customer)->first();
        $invoiceModel = Invoice::where("user_id", $user->id)->where("stripe_invoice_id", $invoice->id)->first();

        if ($invoiceModel) {
            $invoiceModel->stripe_payment_status = "payment_failed";
            $invoiceModel->json_data = json_encode($invoice);  
            $invoiceModel->save();
            $data = [
                "action" => "invoice payment failed"
            ];
        }

        return $data;
        
    }


    public function stripe(Request $request) {

        $data = null;

        $stripe = new \Stripe\StripeClient(config('app.STRIPE_SECRET_KEY'));
        $endpoint_secret = config("app.STRIPE_WEBHOOK_SECRET_KEY");
    
        $payload = @file_get_contents('php://input');
        $sig_header = $request->header('Stripe-Signature');
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        }
    
        if (!$event) {
            http_response_code(400);
            exit();
        }

        // Handle the event

        switch ($event->type) {
            case 'setup_intent.succeeded':
                $setupIntent = $event->data->object; // contains a \Stripe\setupIntent
                $data = $this->handleSetupIntentSucceeded($setupIntent);
                break;
            case 'customer.subscription.created':
                $subscription = $event->data->object; // contains a \Stripe\subscription
                $data = $this->handleSubscriptionUpdated($subscription);
                break;
            case 'customer.subscription.updated':
                $subscription = $event->data->object; // contains a \Stripe\subscription
                $data = $this->handleSubscriptionUpdated($subscription);
                break;
            case 'customer.subscription.deleted':
                $subscription = $event->data->object; // contains a \Stripe\subscription
                $data = $this->handleSubscriptionDeleted($subscription);
                break;
            case 'invoice.created':
                $invoice = $event->data->object; // contains a \Stripe\invoice
                $data = $this->handleInvoiceCreated($invoice);
                break;
            case 'invoice.paid':
                $invoice = $event->data->object; // contains a \Stripe\invoice
                $data = $this->handleInvoicePaid($invoice);
                break;
            case 'invoice.deleted':
                $invoice = $event->data->object; // contains a \Stripe\invoice
                $data = $this->handleInvoiceDeleted($invoice);
                break;
            case 'invoice.payment_action_required':
                $invoice = $event->data->object; // contains a \Stripe\invoice
                $data = $this->handleInvoicePaymentActionRequired($invoice);
                break;
            case 'invoice.payment_failed':
                $invoice = $event->data->object; // contains a \Stripe\invoice
                $data = $this->handleInvoicePaymentFailed($invoice);
                break;
            // // ... handle other event types
            default:
                $data = ["action" => "$event->type not handled"];
        }
            
        
        return response()->json(["status" => "ok", "data" => $data ]);

    }
    
}