<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function markAsRead($id)
    {
        $notification_id = decryptId($id);

        $notification = Notification::find($notification_id);

        if (!$notification) {
            return response()->json(["message" => "Notification not exists!"], 422);
        }

        $notification->update(['read_at' => now()]);

        return response()->json(["message" => "Notification marked as read!"]);
    }

    public function getSingleNotification($user_id)
    {
        $notification = Notification::where('user_id', decryptId($user_id))->whereNull('read_at')->first();

        $notification = $notification ? $notification->getData() : [];

        return response()->json($notification);
    }
}
