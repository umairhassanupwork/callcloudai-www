<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Industry;
use Auth;

use Illuminate\Support\Facades\Crypt;



class IndustryController extends Controller
{
    public function getList(Request $request) {
        
        if($request->q){
            $industries = Industry::where("industry","like","%".$request->q."%")->select('id','industry')->limit(20)->get();
        }else{
            $industries = Industry::select('id','industry')->limit(20)->get();
        }

        return response()->json($industries);
    }
}