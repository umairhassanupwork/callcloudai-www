<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class OTPVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // $optVerified = $request->session()->get('optVerified', false);
        
        $optVerified = session('optVerified', false);
        // if (!$optVerified) {
        //     $data['message'] = 'Need OTP verification!';
        //     return response()->json($data,422);
        // }
        return $next($request);
    }
}
