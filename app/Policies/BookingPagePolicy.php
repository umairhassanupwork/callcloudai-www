<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Workspace;
use App\Models\BookingPage;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookingPagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createBookingPage(User $user, Workspace $workspace)
    {
        if ($user->role === "company") {
            return $workspace->company_id === $user->id;
        } else {
            $usersWorkspaces = $user->workspaces;

            foreach($usersWorkspaces as $w) {
                if ($w->id === $workspace->id ) {
                    return true;
                }
            }

            return false;
        }
    }
    
    public function updateBookingPage(User $user, Workspace $workspace, BookingPage $bookingPage)
    {
        if ($user->role === "company") {
            return $workspace->company_id === $user->id && $bookingPage->workspace_id === $workspace->id;
        } else {
            return $bookingPage->created_by === $user->id;
        }
    }

    public function deleteBookingPage(User $user, Workspace $workspace, BookingPage $bookingPage)
    {
        if ($user->role === "company") {
            return $workspace->company_id === $user->id && $bookingPage->workspace_id === $workspace->id;
        } else {
            return $bookingPage->created_by === $user->id;
        }
    }
    
    public function viewBookingPage(User $user, Workspace $workspace, BookingPage $bookingPage)
    {
        if ($user->role === "company") {
            return $workspace->company_id === $user->id && $bookingPage->workspace_id === $workspace->id;
        } else {
            $usersWorkspaces = $user->workspaces;

            foreach($usersWorkspaces as $w) {
                if ($w->id === $bookingPage->workspace_id) {
                    return true;
                }
            }

            return false;
        }
    }
    

}
