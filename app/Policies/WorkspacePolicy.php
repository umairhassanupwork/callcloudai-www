<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Workspace;
use Illuminate\Auth\Access\HandlesAuthorization;

class WorkspacePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createWorkspace(User $user)
    {
        return $user->role === "company";
    }
    
    public function updateWorkspace(User $user, Workspace $workspace)
    {
        return $user->role === "company" && $workspace->company_id === $user->id;
    }

    public function deleteWorkspace(User $user, Workspace $workspace)
    {
        return $user->role === "company"  && $workspace->company_id === $user->id;
    }
    
    public function viewWorkspace(User $user, Workspace $workspace)
    {
        if ($user->role === "company") {
            return $workspace->company_id === $user->id;
        } else {
            $usersWorkspaces = $user->workspaces;

            foreach($usersWorkspaces as $w) {
                if ($w->id === $workspace->id ) {
                    return true;
                }
            }

            return false;
        }
    }
    

}
