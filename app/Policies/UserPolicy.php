<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Workspace;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    
    
    public function updateProfile(User $authUser, User $user)
    {
        if ($authUser->id == $user->id) {

            return true;

        } else {

            $authUsersWorkspaces = $authUser->getCompanyWorkspaces();
            $usersWorkspaces = $user->workspaces;

            $authUsersWorkspacesIds = [];
            foreach ($authUsersWorkspaces as $workspace) {
                $authUsersWorkspacesIds[] = $workspace->id;
            }
            
            $usersWorkspacesIds = [];
            foreach ($usersWorkspaces as $workspace) {
                $usersWorkspacesIds[] = $workspace->id;
            }

            $intersect = array_intersect($usersWorkspacesIds, $authUsersWorkspacesIds);

            if (count($intersect) > 0) {
                return true;
            }

            return false;

        }
    }

    
    

}
