<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\WaitList;


class WaitListSubscribed extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $waitList;
    public $appURL;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(WaitList $waitList)
    {
        $this->waitList = $waitList;
        $this->appURL = config('app.url');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.waitListSubscribed');
    }
}
