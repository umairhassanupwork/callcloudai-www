<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\User;


class WorkspaceInvitation extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $invitationUrl;
    public $existingUser;
    public $workspace;
    public $forgotPasswordLink;
    public $loginLink;
    public $appURL;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $invitationUrl, $existingUser, $workspace)
    {
        $this->user = $user;
        $this->invitationUrl = $invitationUrl;
        $this->existingUser = $existingUser;
        $this->workspace = $workspace;
        $this->forgotPasswordLink = config('app.frontend_url')."/forgot-password";
        $this->loginLink = config('app.frontend_url')."/login";
        $this->appURL = config('app.url');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $workspaceName = $this->workspace->name;
        return $this->view('emails.workspaceInvitation')->subject("Invited to CallCloud Workspace: $workspaceName");
    }
}
