<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\User;


class SubscriptionCanceled extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $appURL;
    public $billingURL;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->appURL = config('app.url');
        $this->billingURL = config('app.frontend_url')."/profile?active_tab=billing";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.subscriptionCanceled')->subject("Subscription Canceled!");
    }
}
