<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\User;


class FreePlanWillExpireToday extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $appURL;
    public $willExpireAtDate;
    public $billingURL;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $willExpireAtDate)
    {
        $this->user = $user;
        $this->appURL = config('app.url');
        $this->willExpireAtDate = $willExpireAtDate;
        $this->billingURL = config('app.frontend_url')."/profile?active_tab=billing";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.freePlanWillExpireToday')->subject("Free Plan Will Expire Today!");
    }
}
