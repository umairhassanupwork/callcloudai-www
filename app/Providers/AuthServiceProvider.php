<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use App\Policies\WorkspacePolicy;
use App\Policies\UserPolicy;
use App\Policies\BookingPagePolicy;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        'App\Models\Workspace' => 'App\Policies\WorkspacePolicy',
        'App\Models\User' => 'App\Policies\UserPolicy',
        'App\Models\BookingPage' => 'App\Policies\BookingPagePolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('create-booking-page', [BookingPagePolicy::class, 'createBookingPage']);
        Gate::define('view-booking-page', [BookingPagePolicy::class, 'viewBookingPage']);
        Gate::define('update-booking-page', [BookingPagePolicy::class, 'updateBookingPage']);
        Gate::define('delete-booking-page', [BookingPagePolicy::class, 'deleteBookingPage']);
        
        Gate::define('create-workspace', [WorkspacePolicy::class, 'createWorkspace']);
        Gate::define('view-workspace', [WorkspacePolicy::class, 'viewWorkspace']);
        Gate::define('update-workspace', [WorkspacePolicy::class, 'updateWorkspace']);
        Gate::define('delete-workspace', [WorkspacePolicy::class, 'deleteWorkspace']);
        
        
        Gate::define('update-profile', [UserPolicy::class, 'updateProfile']);


        //
    }
}
