## Info about software requirements

This is laravel 9 so need latest php 8 and mysql 8. Also we will need process management tool like supervisor for manage queue.

## Info about project setup

Perform below steps after clone.

-   composer install
-   copy .env.example to .env and change variables like DB, SMTP, Domains
-   php artisan key:generate
-   php artisan migrate
-   php artisan queue:work with supervisor
-   php artisan db:seed for seed data in Database (One time only per each item in seed file)
-   php artisan storage:link for create symbolic link (One time only)

Stripe needs webhook setup
