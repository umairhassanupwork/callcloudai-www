<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>CallCloud | Coming Soon</title>
<!-- Stylesheets -->
<link href="{{URL::asset('/coming-soon/css/responsive.css')}}" rel="stylesheet">
<link href="{{URL::asset('/coming-soon/css/style.css')}}" rel="stylesheet">
<link href="{{URL::asset('/coming-soon/css/bootstrap.min.css')}}" rel="stylesheet">






<link rel="shortcut icon" href="{{URL::asset('/coming-soon/images/favicon.svg')}}" type="image/x-icon">
<link rel="icon" href="{{URL::asset('/coming-soon/images/favicon.svg')}}" type="image/x-icon">


<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->

</head>
<body>

<div class="page-wrapper">

    <!-- Preloader -->
    <div class="preloader">
        <!-- <img src="images/favicon.svg"> -->
        <img src="{{URL::asset('/coming-soon/images/favicon.svg')}}" >

    </div>


    <!-- Coming Soon -->
    <section class="coming-soon">
        <div class="anim-icons">
            <span class="icon icon-dots-2 bounce-x"></span>
        </div>
        <div class="content">
            <div class="content-inner">
                <div class="auto-container">
                    <div class="logo loaded wow fadeInUp"><a href="/"><img src="{{URL::asset('/coming-soon/images/logo.svg')}}" alt=""></a></div>
                    <figure class="image wow fadeInUp" data-wow-delay='200ms'><img src="{{URL::asset('/coming-soon/images/resource/coming-soon.svg')}}" alt=""></figure>
                    <div class="text  wow fadeInUp" data-wow-delay='400ms'>Easily manage your at-home phone sales team.</div>

                    <div class="subscribe-form wow fadeInUp" data-wow-delay='600ms'>
                        <form method="post" action="wait-list-thank-you.html">
                            <div class="form-group">
                                <input id="email-input" type="email" name="email" class="bg-light" value="" placeholder="Enter your best email" required="">
                                <button type="submit" class="theme-btn btn-style-one">Join the wait list</button>
                            </div>
                        </form>
                        <div class="ty-msg">Thanks! You'll get our updates</div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End Coming Soon -->


</div><!-- Page Wrapper -->

<script src="{{URL::asset('/coming-soon/js/jquery.js')}}"></script>
<script src="{{URL::asset('/coming-soon/js/popper.min.js')}}"></script>
<script src="{{URL::asset('/coming-soon/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('/coming-soon/js/wow.js')}}"></script>
<script src="{{URL::asset('/coming-soon/js/script.js')}}"></script>


<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.subscribe-form form').submit(function(e){
        $(this).parents('.subscribe-form').find('.ty-msg').addClass("d-block animated fadeInUp");
        console.log($("#email-input").val())
        event.preventDefault();
        $.post("coming-soon-wait-list",
        {
            email: $("#email-input").val()
        },
        function(data, status){
            console.log(status);
        });

        $(this).hide();
    })
</script>
</body>
</html>