@component('mail::layout')

{{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
        <div style="display: flex; align-items: center; justify-content: center">
            <img height="20px" width="35px" style="padding-right: 10px;" src="{{URL::asset('/coming-soon/images/logo.png')}}" > 
            {{ config('app.name') }}
        </div>
        
        @endcomponent
    @endslot


{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Hello!')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    // $color = match ($level) {
    //     'success', 'error' => $level,
    //     default => 'primary',
    // };
//     @component('mail::button', ['url' => $actionUrl, 'color' => $color])
// {{ $actionText }}
// @endcomponent
?>
<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table border="0" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td>
<a href="{{ $actionUrl}} " style="display: inline-block;border-radius: 50px; font-weight: 600; font-size: 14px; line-height: 100%; padding: 16px 40px; --text-opacity: 1; color: #ffffff; background-color:#433cd8 ; text-decoration: none; margin-bottom:20px;">{{ $actionText }}</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>


@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else

@lang('Best wishes'),<br>
{{ config('app.name') }} Team
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@component('mail::subcopy')
@lang(
    "If you're having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser:',
    [
        'actionText' => $actionText,
    ]
) <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endcomponent
@endslot
@endisset

{{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
